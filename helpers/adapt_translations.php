<?php

/*
 * Adapter les fichiers de langue de l’ancien Spip Loader, vers celui-là.
 * En supposant les fichiers dans lang/tradloader_fr.php ...
 */

$files = new FilesystemIterator(__DIR__ . '/../lang', FilesystemIterator::SKIP_DOTS);
$replacements = [
	'$GLOBALS[$GLOBALS[\'idx_lang\']] = array(' => 'return [',
	');' => '];',
	"// This is a SPIP language file  --  Ceci est un fichier langue de SPIP\n" => '',
	"// ** ne pas modifier le fichier **\n" => '',
	"// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_outils_/spip_loader/trunk/" => '',
	#"<?php" => '',
	"?>" => '',
	"&Eacute ;" => "E", // double faute !
	"&agrave ;" => "à",
	"<br />" => "",
	"<b>" => "<strong>",
	"</b>" => "</strong>",
	"<i>" => "<code>",
	"</i>" => "</code>",
	"Telnet" => "SSH",
];

# langues disponibles
$langues = array (
	'ar' => "&#1593;&#1585;&#1576;&#1610;",
	'ast' => "asturianu",
	'br' => "brezhoneg",
	'ca' => "catal&#224;",
	'cs' => "&#269;e&#353;tina",
	'de' => "Deutsch",
	'en' => "English",
	'eo' => "Esperanto",
	'es' => "Espa&#241;ol",
	'eu' => "euskara",
	'fa' => "&#1601;&#1575;&#1585;&#1587;&#1609;",
	'fr' => "fran&#231;ais",
	'fr_tu' => "fran&#231;ais copain",
	'gl' => "galego",
	'hr' => "hrvatski",
	'id' => "Indonesia",
	'it' => "italiano",
	'km' => "Cambodian",
	'lb' => "L&euml;tzebuergesch",
	'nap' => "napulitano",
	'nl' => "Nederlands",
	'oc_lnc' => "&ograve;c lengadocian",
	'oc_ni' => "&ograve;c ni&ccedil;ard",
	'pt_br' => "Portugu&#234;s do Brasil",
	'ro' => "rom&#226;n&#259;",
	'ru' => "Русский",
	'sk' => "sloven&#269;ina",	// (Slovakia)
	'sv' => "svenska",
	'tr' => "T&#252;rk&#231;e",
	'wa' => "walon",
	'zh_tw' => "&#21488;&#28771;&#20013;&#25991;", // chinois taiwan (ecr. traditionnelle)
);

$i = 0;
foreach ($files as $file) {
	$locale = substr($file->getBasename('.php'), strlen('tradloader_'));
	$content = file_get_contents($file);
	$content = str_replace(array_keys($replacements), $replacements, $content);
	$content = preg_replace('#' . preg_quote('// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=') . '.*\n*#', "\n", $content);
	// Symfony Translate conseille %...% pour les variables.
	$content = preg_replace('#@([a-z0-9]+)@#', '%$1%', $content);
	$dest = __DIR__ . '/../translations/spip_loader.' . $locale . '.php';
	if (file_exists($dest)) {
		unlink($dest);
	}
	$langue = html_entity_decode($langues[$locale] ?? $locale);
	$content = str_replace("return [", "return [\n\t'nom_langue' => '$langue',", $content);
	file_put_contents($dest, $content);
	$i++;
}
echo  "$i fichiers transformés.";