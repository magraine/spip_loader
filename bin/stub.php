<?php
if (in_array('phar', stream_get_wrappers()) && class_exists('Phar', 0)) {
	Phar::interceptFileFuncs();
	set_include_path('phar://' . __FILE__ . PATH_SEPARATOR . get_include_path());
#Phar::webPhar(null, '');
	include 'phar://' . __FILE__ . '/src/Bootstrap.php';
	return;
}
// Can’t use phar…
header('HTTP/1.0 500 Internal Server Error');
echo "<html>\n <head>\n  <title>Internal Server Error<title>\n </head>\n <body>\n  <h1>500 - Internal Server Error</h1>\n </body>\n</html>";
exit;
__HALT_COMPILER(); ?>