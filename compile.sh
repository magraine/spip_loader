#!/usr/bin/env sh
# sh compile.sh
# sh compile.sh dev
STATE=$1
CONFIG=box.json
if [[ ${STATE} -eq "dev" ]]; then
    CONFIG=box.dev.json
fi

php box.phar compile --config=${CONFIG}
php bin/post_box_build
