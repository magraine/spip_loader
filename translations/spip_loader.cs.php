<?php

return [
	'nom_langue' => 'čeština',

	// B
	'bouton_suivant' => 'Instalovat >>',

	// C
	'ce_repertoire' => 'tento adresář',

	// D
	'donnees_incorrectes' => '<h4>Chybné údaje. Prosím zkuste
znovu nebo použijte manuální instalaci.</h4>
  <p>Error : %erreur%</p>',
	'du_repertoire' => 'adresář',

	// E
	'echec_chargement' => '<h4>Stažení se nepovedlo. Prosím
zkuste znovu nebo použijte manuální instalaci.</h4>',

	// T
	'texte_intro' => '<p><strong>Vítejte v automatické
instalační proceduře %paquet%u.</strong> </p><p>Program nejprve zkontroluje
oprávnění pro aktuální adresář a potom do něj začne stahovat
instalační soubory %paquet%u.</p> <p>Pro pokračování stiskněte tlačítko.</p>', # MODIF
	'texte_preliminaire' => '<h2>Přípravný rok: <strong>Nastavte
přístupová oprávnění</strong></h2> <p><strong>Není možné zapisovat do 
aktuálního adresáře.</strong></p> <p>Pro změnu přístupových práv do adresáře
kam instalujete %paquet%, použijte svého FTP klienta. KOnkrétní postup je popsán
v Uživatelské příručce. Zvolte jeden postup:</p> <ul><li><strong>Pokud
máte grafického FTP klienta</strong>, nastavte oprávnění tak,
aby do adresáře mohli všichni zapisovat.</li>
<li><strong>Pokud máte
textového FTP klienta</strong>, nastavte přístupová oprávnění 
adresáře na hodnotu %chmod%.</li>
<li><strong>Pokud používáte telnet/ssh přístup</strong>,
spusťte příkaz <code>chmod %chmod% current_directory</code>.</li> </ul>
<p>Až budete hotovi,<strong><a
href=\'%href%\'>obnovte tuto
stránku</a></strong>, aby mohlo začít stahování a instalace %paquet%u. </p><p>Bude-li se
tato chyba opakovat,budete muset použít
manuální instalační postup (stáhnout instalační soubory pomocí FTP).</p>',
	'titre' => 'Probíhá stahování %paquet%'
];
