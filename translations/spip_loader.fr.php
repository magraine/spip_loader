<?php

return [
	'nom_langue' => 'français',

	// B
	'bouton_suivant' => 'Commencer l’installation >>',
	'bouton_suivant_maj' => 'Lancer la mise à jour >>',

	// C
	'ce_repertoire' => 'de ce répertoire',

	// D
	'donnees_incorrectes' => '<h4>Données incorrectes. Veuillez réessayer, ou utiliser l’installation manuelle.</h4>
		<p>Erreur produite : %erreur%</p>',
	'du_repertoire' => 'du répertoire',

	// E
	'echec_chargement' => '<h4>Le chargement a échoué. Veuillez réessayer, ou utiliser l’installation manuelle.</h4>',
	'echec_php' => 'Votre version de PHP %php1% n’est pas compatible avec cette version de SPIP qui nécessite au moins PHP %php2%.',

	// S
	'spip_loader_maj' => 'La version %version% de spip_loader.php est disponible.',

	// T
	'texte_intro' => '<p>Le programme va télécharger les fichiers de %paquet% à l’intérieur %dest%.</p>',
	'texte_preliminaire' => '<h2>Préliminaire : <strong>Régler les droits d’accès</strong></h2>
<p><strong>Le répertoire courant n’est pas accessible en écriture.</strong></p>
<p>Pour y remédier, utilisez votre client FTP afin de régler les droits d’accès à ce répertoire (répertoire d’installation de %paquet%).
La procédure est expliquée en détail dans le guide d’installation. Au choix :</p>
<ul>
<li><strong>Si vous avez un client FTP graphique</strong>, réglez les propriétés du répertoire courant afin qu’il soit accessible en écriture pour tous.</li>
<li><strong>Si votre client FTP est en mode texte</strong>, changez le mode du répertoire à la valeur %chmod%.</li>
<li><strong>Si vous avez un accès SSH</strong>, faites un <code>chmod %chmod% repertoire_courant</code>.</li>
</ul>
<p>Une fois cette manipulation effectuée, vous pourrez <strong><a href=\'%href%\'>recharger cette page</a></strong> afin de commencer le téléchargement puis l’installation.</p>
<p>Si l’erreur persiste, vous devrez passer par la procédure d’installation classique (téléchargement de tous les fichiers par FTP).</p>',
	'titre' => 'Téléchargement de %paquet%',
	'titre_maj' => 'Mise à jour de %paquet%',
	'titre_version_courante' => 'Version actuellement installée : ',
	'titre_version_future' => 'Installation de la version : '
];
