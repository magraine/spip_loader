<?php

return [
	'nom_langue' => 'walon',

	// B
	'bouton_suivant' => 'Ataker l’ astalaedje >>',

	// C
	'ce_repertoire' => 'di ç’ ridant ci',

	// D
	'donnees_incorrectes' => '<h4>Dinêyes nén corekes. Rissayîz s’ i vs plait, oudonbén fijhoz l’ astalaedje al mwin.</h4>
  <p>Aroke k’ i gn a yeu: %erreur%</p>',

	// T
	'texte_preliminaire' => '<h2>Préliminaire : <strong>Régler les droits d’accès</strong></h2>
  <p><strong>Le répertoire courant n’est pas accessible en écriture.</strong></p>
  <p>Pour y remédier, utilisez votre client FTP afin de régler les droits d’accès
  à ce répertoire (répertoire d’installation de %paquet%).
  La procédure est expliquée en détail dans le guide d’installation. Au choix :</p>
  <ul>
  <li><strong>Si vous avez un client FTP graphique</strong>, réglez les propriétés du répertoire courant
  afin qu’il soit accessible en écriture pour tous.</li>
  <li><strong>Si votre client FTP est en mode texte</strong>, changez le mode du répertoire à la valeur %chmod%.</li>
  <li><strong>Si vous avez un accès SSH</strong>, faites un <code>chmod %chmod% repertoire_courant</code>.</li>
  </ul>
  <p>Une fois cette manipulation effectuée, vous pourrez <strong><a href=\'%href%\'>recharger cette page</a></strong>
  afin de commencer le téléchargement puis l’installation.</p>
  <p>Si l’erreur persiste, vous devrez passer par la procédure d’installation classique
  (téléchargement de tous les fichiers par FTP).</p>'
];
