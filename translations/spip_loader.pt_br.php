<?php

return [
	'nom_langue' => 'Português do Brasil',

	// B
	'bouton_suivant' => 'Iniciar a instalação &gt;&gt;',
	'bouton_suivant_maj' => 'Iniciar a atualização >>',

	// C
	'ce_repertoire' => 'deste diretório',

	// D
	'donnees_incorrectes' => '<h4>Dados incorretos. Tente novamente, ou use a instalação manual.</h4>
<p>Erro: %erreur%</p>',
	'du_repertoire' => 'do diretório',

	// E
	'echec_chargement' => '<h4>A transferência falhou. Tente novamente, ou use a instalação manual.</h4>',
	'echec_php' => 'A sua versão de PHP %php1% não é compatível com esta versão do SPIP, que requer pelo menos o PHP %php2%.',

	// S
	'spip_loader_maj' => 'A versão %version% do spip_loader.php está disponível.',

	// T
	'texte_intro' => '<p>O programa vai transferir os arquivos do %paquet% para o %dest%.</p>',
	'texte_preliminaire' => '<h2>Preliminar: <strong>Ajustar os direitos de acesso</strong></h2>
<p><strong>O diretório atual não está acessível para escrita.</strong></p>
<p>Para solucionar o problema, use o seu programa de FTP para ajustar os direitos de acesso a este diretório (diretório de instalação do %paquet%).
O procedimento é explicado em detalhe no guia de instalação. Opções:</p>
<ul>
<li><strong>Se você possui um programa de FTP gráfico</strong>, ajuste as propriedades do diretório atual para que ele seja acessível para escrita para todos.</li>
<li><strong>Se o seu programa de FTP é em modo caracter</strong>, altere o modo do diretório para o valor %chmod%.</li>
<li><strong>Se você tem um acesso SSH</strong>, faça um <code>chmod %chmod% diretorio_atual</code>.</li>
</ul>
<p>Após fazer estes ajustes, você poderá <strong><a href=\'%href%\'>recarregar esta página</a></strong> para iniciar a transeferência e posterior instalação.</p>
<p>Se o erro persistir, você deverá optar pela instalação manual clássica
(transferência de todos os arquivos por FTP).</p>',
	'titre' => 'Transferência do %paquet%',
	'titre_maj' => 'Atualização de %paquet%',
	'titre_version_courante' => 'Versão instalada atualmante: ',
	'titre_version_future' => 'Instalação da versão: '
];
