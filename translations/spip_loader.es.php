<?php

return [
	'nom_langue' => 'Español',

	// B
	'bouton_suivant' => 'Empezar la instalación >>',
	'bouton_suivant_maj' => 'Lanzar la actualización >>',

	// C
	'ce_repertoire' => 'de esta carpeta',

	// D
	'donnees_incorrectes' => '<h4>Datos incorrectos. Vuelve a probar, o utiliza la instalación manual.</h4>
  <p>Erreur produite: %erreur%</p>',
	'du_repertoire' => 'de la carpeta',

	// E
	'echec_chargement' => '<h4>La descarga falló. Vuelve a probar, o utiliza la instalación manual.</h4>',
	'echec_php' => 'Tu versión de PHP %php1% no es compatible con esta versión de SPIP que requiere al menos PHP %php2%.',

	// S
	'spip_loader_maj' => 'La versión %version% de spip_loader.php está disponible.',

	// T
	'texte_intro' => '<p>El programa va a descargar los archivos de %paquet% dentro de %dest%.</p>',
	'texte_preliminaire' => '<h2>Preliminar: <strong>Ajustar los derechos de acceso</strong></h2>
<p><strong>La carpeta actual no está accesible en modo escritura.</strong></p>
<p>Para resolverlo, utiliza tu cliente FTP y ajusta los permisos de acceso
 a esta carpeta (carpeta de instalación de %paquet%).
El proceso está explicado con detalle en la guía de instalación. Según el caso:</p>
<ul>
<li><strong>Si tienes un cliente FTP gráfico</strong>, ajusta las propiedades de la carpeta actual para que esté accesible en escritura para todos.</li>
<li><strong>Si tu cliente FTP funciona en modo texto</strong>, cambia el modo de la carpeta al valor %chmod%.</li>
<li><strong>Si tienes un acceso ssh o SSH</strong>, ejecuta un <code>chmod %chmod% carpeta_actual</code>./li>
</ul>
<p>Una vez efectuado este cambio, podrás <strong><a href=\'%href%\'>volver a cargar esta página</a></strong> para empezar la descarga y luego la instalación.</p>
<p>Si el error persiste, deberás pasar por el procedimiento de instalación clásico (subir todos los archivos por FTP).</p>',
	'titre' => 'Descarga de %paquet%',
	'titre_maj' => 'Actualización de %paquet%',
	'titre_version_courante' => 'Versión actualmente instalada: ',
	'titre_version_future' => 'Instalación de la versión: '
];
