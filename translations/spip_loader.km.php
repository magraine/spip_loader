<?php

return [
	'nom_langue' => 'Cambodian',

	// B
	'bouton_suivant' => 'ផ្តើម ការតំលើង >>',

	// C
	'ce_repertoire' => 'ថតឯកសារនេះ',

	// D
	'du_repertoire' => 'ថតឯកសារ',

	// T
	'texte_preliminaire' => '<h2>Preliminary step: <strong>Set the access permissions</strong></h2>
 <p><strong>It is not possible to write to the
current directory.</strong></p>
 <p>To change the permissions of the directory in
which you are installing %paquet% use your FTP client. The procedure is explained in detail in the Installation Guide. Choose between:</p>
 <ul>
 <li><strong>If you have an FTP client with a graphical interface</strong>, set the permissions
of the directory to make it open for everyone to write to it.</li>
 <li><strong>If you have an FTP client with a text interface</strong>, change the permissions of the directory to the value %chmod%.</li>
 <li><strong>If you are using a SSH access</strong>,
execute the command <code>chmod %chmod% current_directory</code>.</li>
 </ul>
<p>Once this has been done, please <strong><a href=\'%href%\'>reload this page</a></strong>
 to start to download and install SPIP.</p>
 <p>If you continue to receive this error notification, you will need to use the manualinstallation method
 (downloading the SPIP files by FTP) instead.</p>',
	'titre' => 'ទំនាញយក នៃ %paquet%'
];
