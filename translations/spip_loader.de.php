<?php

return [
	'nom_langue' => 'Deutsch',

	// B
	'bouton_suivant' => 'Installation starten >>',

	// C
	'ce_repertoire' => 'dieses Ordners',

	// D
	'donnees_incorrectes' => '<h4>Falsche Daten. Bitte versuchen Sie es erneut, oder installieren Sie SPIP manuell.</h4>
  <p>Fehler: %erreur%</p>',
	'du_repertoire' => 'des Ordners',

	// E
	'echec_chargement' => '<h4>Laden nicht möglich. Bitte versuchen Sie es erneut, oder installieren Sie SPIP manuell.</h4>',

	// T
	'texte_intro' => '<p><strong>Willkommen bei der automatischen Installation von %paquet%.</strong>
  <p>Das System wird zunächst die Zugriffsrechte auf das aktuelle Verzeichnis prüfen,
  und dann die Installation von %paquet% in diesem Verzeichnis beginnen.
  <p>Bitte klicken Sie auf den Knopf Weiter, um fortzufahren.', # MODIF
	'texte_preliminaire' => '<h2>Vorbereitung: <strong>Zugriffsrechte einstellen</strong></h2>
<p><strong>In das aktuelle Verzeichnis kann nicht geschrieben werden.</strong>
<p>Bitte verwenden Sie Ihr FTP-Programm, um die Zugriffsrechte für das
 Installationsverzeichnis von %paquet% einzustellen. Die Installationsanleitung erläutert die Vorgehensweise:/p>
<ul>
<li><strong>Mit einem grafischen FTP-Client</strong>, stellen Sie die Zugriffsrechte so ein, dass jeder in das Verzeichnis schreiben darf.</li>
<li><strong>Wenn Sie einen textbasierten FTP-Client verwenden</strong>, ändern Sie den Modus des Verzeichnis nach %chmod%.</strong><></li>
<li><strong>Wenn Sie einen SSHzugang haben</strong>, führen Sie <code>chmod %chmod% aktuelles_verzeichnis</code> aus.</li>
</ul>
<p>Wenn Sie diese Änderung durchgeführt haben, können Sie <strong><a href=\'%href%\'>diese Seite neu laden</a></strong>, um SPIP herunterzuladen und die Installation zu beginnen.</p>
<p>Falls der Fehler weiter auftritt, können Sie die klassische Installation durchführen (Kopieren Sie alle Dateien per FTP auf Ihren Server).</p>',
	'titre' => 'Herunterladen von %paquet%'
];
