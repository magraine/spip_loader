<?php

return [
	'nom_langue' => 'italiano',

	// B
	'bouton_suivant' => 'Inizia l’installazione >>',
	'bouton_suivant_maj' => 'Avviare l’aggiornamento >>',

	// C
	'ce_repertoire' => 'di questa cartella',

	// D
	'donnees_incorrectes' => '<h4>Dati corrotti. Riprova o procedi all’installazione manuale.</h4>',
	'du_repertoire' => 'della cartella',

	// E
	'echec_chargement' => '<h4>Il caricamento è fallito. Riprova, o utilizza l’installazione manuale.</h4>',
	'echec_php' => 'La vostra versione di PHP %php1% non è compatibile con questa versione di SPIP. È necessario avere almeno PHP %php2%.',

	// S
	'spip_loader_maj' => 'La versione %version% di spip_loader.php è ora disponibile.',

	// T
	'texte_intro' => '<p><strong>Benvenuto nella procedura d’installazione automatica di %paquet%.</strong></p>
  <p>Il sistema ha verificato i permessi di scrittura per la cartella corrente.</p>
<p>Il programma procederà ora al download dei dati %paquet% all’interno %dest%.</p>
  <p>Clicca sul tasto per continuare.</p>', # RELIRE
	'texte_preliminaire' => '<h2>Configurazione preliminare: 
  <strong>Regolare i permessi di scrittura</strong></h2>
  <p><strong>La cartella corrente non è accessibile in scrittura.</strong></p>
  <p>Modificare i permessi della cartella corrente (la cartella in cui verrà installato %paquet%) utilizzando un client FTP.
  La procedura è spiegata in dettaglio all’interno della guida di installazione. Alcuni consigli:</p>
  <ul>
  <li><strong>Se hai un client FTP con interfaccia grafica</strong>, modifica le proprietà della cartella corrente
  in modo da essere accessibile in scrittura da tutti.</li>
  <li><strong>Se hai un client FTP in modalità testo</strong>, modifica i permessi della cartella corrente impostandoli a %chmod%.</li>
  <li><strong>Se utilizzi un accesso SSH</strong>, esegui il comando <code>chmod %chmod% nome_cartella</code>.</li>
  </ul>
  <p>Una volta effettuata questa modifica, devi <strong><a href=\'%href%\'>ricaricare questa pagina</a></strong>
  per far partire il download e l’installazione.</p>
  <p>Se l’errore persiste, sarà necessario passare alla procedura di installazione classica
  (caricamento di tutti i file di SPIP via FTP).</p>',
	'titre' => 'Download di %paquet%',
	'titre_maj' => 'Aggiornamento di %paquet%',
	'titre_version_courante' => 'Ultima versione installata : ',
	'titre_version_future' => 'Installazione della versione : '
];
