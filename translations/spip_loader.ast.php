<?php

return [
	'nom_langue' => 'asturianu',

	// B
	'bouton_suivant' => 'Entamar l’asitiamientu >>',

	// C
	'ce_repertoire' => 'd’esti direutoriu',

	// D
	'donnees_incorrectes' => '<h4>Datos incorreutos. Vuelva a intentalo, o use l’asitiamientu manual.</h4>
  <p>Fallu producíu: %erreur%</p>',
	'du_repertoire' => 'del direutoriu',

	// E
	'echec_chargement' => '<h4>Falló la carga. Vuelva a intentalo, o use l’asitiamientu manual.</h4>',

	// T
	'texte_intro' => '<p><strong>Afáyese nel procesu d’asitiamientu automáticu de %paquet%.</strong></p>
  <p>El sistema ya comprobó los permisos d’acesu al direutoriu actual.
  Agora va entamar la descarga de los datos de %paquet% dientro de %dest%.</p>
  <p>Calque nel botón siguiente pa continuar.</p>', # MODIF
	'texte_preliminaire' => '<h2>Previu: <strong>Regular los derechos d’accesu</strong></h2>
  <p><strong>El direutoriu actual nun tien accesu pa escritura.</strong></p>
  <p>Pa igualo, usa’l programa cliente FTP pa regular los derechos d’accesu
  a esti direutoriu (direutoriu d’instalación de %paquet%).
  El procedimientu detalláu s’esplica na guía d’instalación. A escoyer:</p>
  <ul>
  <li><strong>Si tienes un cliente FTP gráficu</strong>, regula les propiedaes del direutoriu actual
  pa que tenga accesu d’escritura pa toos.</li>
  <li><strong>Si’l cliente FTP ye en mou testu</strong>, camuda el mou del direutoriu al valor %chmod%.</li>
  <li><strong>Si tienes accesu SSH</strong>, fai un <code>chmod %chmod% direutoriu_actual</code>.</li>
  </ul>
  <p>De magar tea fecha esta operación, podrás <strong><a href=\'%href%\'>recargar esta páxina</a></strong>
  pa entamar la descarga y darréu la instalación.</p>
  <p>Si siguiere l’error, tendrás que pasar al procesu d’instalación clásicu
  (carga de tolos ficheros per FTP).</p>',
	'titre' => 'Descarga de %paquet%'
];
