<?php

return [
	'nom_langue' => 'Lëtzebuergesch',

	// B
	'bouton_suivant' => 'Installatioun ufänken >>',

	// C
	'ce_repertoire' => 'vun dësem Dossier',

	// D
	'donnees_incorrectes' => '<h4>Falsch Daten. Probéiert nach eng Kéier, oder benotzt déi manuell Installatioun.</h4>
<p>Fehler: %erreur%</p>',
	'du_repertoire' => 'vum Dossier',

	// E
	'echec_chargement' => '<h4>D’Lueden huet nët fonktionnéiert. Probéiert nach eng Kéier, oder benotzt déi manuell Installatioun.</h4>',

	// T
	'texte_intro' => '<p><strong>Wëllkom bei der automatëscher Installatiouns-Prozedur vun %paquet%.</strong>
<p>De System huet d’Zougangs-Rechter um aktuellen Dossier kontrolléiert.
Hien lued elo d’%paquet%-Daten an den Dossier %dest%.</p>
<p>Drëckt op den Knäppchen "Weider".</p>', # MODIF
	'texte_preliminaire' => '<h2>Fir d’éischt: <strong>Zougangs-Rechter astellen</strong><h2>
<p><strong>Den aktuellen Dossier kann nët beschriwen ginn.</strong></p>
<p>Fir dat ze verbesseren, benotzt ären FTP-Client fir d’Zougansrechter vun dësem Dossier anzestellen (wou %paquet% installéiert gëtt).
D’Prozedur ass am Detail am Installatiouns-Guide beschriwen. Zum Beispill:</p>
<ul>
<li><strong>Wann dir e graphëschen FTP-Client hutt</strong>, regléiert d’Rechter vum aktuellen Dossier sou dat en fir jiddfereen beschreiwbar ass.</li>
<li><strong>Wann dir een FTP-Client mat Text-Modus hutt</strong>, ännert d’Rechter vum Dossier op d’Valeur %chmod%.</li>
<li><strong>Wann dir SSH fuert</strong>, maacht een <code>chmod %chmod% aktuellen_dossier</code>.</li>
</ul>
<p>Wann dës Ännerung gemaach ass, da kënnt dir <strong><a href=\'%href%\'>dës Säit nei lueden</a></strong>
an d’Installatioun ufänken.</p>
<p>Wann de Fehler bestoë bleiwt da musst dir déi klassësch Installatiouns-Prozedur benotzen (all d’Dateien per FTP op de Server lueden).</p>',
	'titre' => '%paquet% lueden'
];
