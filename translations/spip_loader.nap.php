<?php

return [
	'nom_langue' => 'napulitano',

	// B
	'bouton_suivant' => 'Accummenciate â nstallazzione >>',

	// C
	'ce_repertoire' => '’e chistu repertorio',

	// D
	'donnees_incorrectes' => '<h4>’E date nun songo currette. Pruvate n’ata vota, o ausate ’a nstallazzione manuale.</h4>
  <p>Arrore produtto: %erreur%</p>',
	'du_repertoire' => 'd’’o repertorio',

	// E
	'echec_chargement' => '<h4>’O carrecamento facette fetècchia. Pruvate n’ata vota, o ausate â nstallazzione manuale.</h4>',

	// T
	'texte_intro' => '<p><strong>Bemmenute â procedura ’e nstallazzione automateca ’e %paquet%.</strong></p>
  <p>’O sistema a valedato ’e deritte d’acciesso ô repertorio currentt.
  Mommò accummenciarrà ’o scarrecamento d’’e date %paquet% à l’intérieur %dest%.</p>
  <p>sprimmite ô buttone ca vene pe ccuntinuà.</p>', # MODIF
	'titre' => 'Scarrecamento ’e %paquet%'
];
