<?php

return [
	'nom_langue' => 'Nederlands',

	// B
	'bouton_suivant' => 'Met de installatie beginnen >>',
	'bouton_suivant_maj' => 'De aanpassing starten >>',

	// C
	'ce_repertoire' => 'van deze lijst',

	// D
	'donnees_incorrectes' => '<h4>Incorrecte gegevens. Probeer opnieuw of gebruik de handmatige installatie.</h4>
  <p>Geproduceerde fout:  %erreur%</p>',
	'du_repertoire' => 'van de lijst',

	// E
	'echec_chargement' => '<h4>Het downloaden is niet geslaagd. Gelieve opnieuw proberen, of de handmatige installatie gebruiken.</h4>',
	'echec_php' => 'Jouw versie van PHP %php1% is niet compatibel met deze versie van SPIP die ten minste PHP %php2% vereist.',

	// S
	'spip_loader_maj' => 'Versie %version% van spip_loader.php is beschikbaar.',

	// T
	'texte_intro' => '<p>Het programma gaat de bestanden van %paquet% downloaden in %dest%.</p>',
	'texte_preliminaire' => '<h2>Voorafgaand: <strong>De toegangsrechten regelen</strong></h2>
  <p><strong>In de huidige map kan niet worden geschreven.</strong></p>
  <p>Om te verhelpen, gebruikt je een FTP client waarmee je de toegangsrechten van deze map instelt (installatiemap van %paquet%).
  De procedure wordt in detail uitgelegd in de installatiegids. Je kunt kiezen uit:</p>
  <ul>
  <li><strong>Als je een grafische FTP client hebt</strong>, stel dan de rechten van de huidige map in zodat iedereen erin kan schrijven.</li>
  <li><strong>Heb je een FTP client in tekst modus</strong>, stel de rechten van de map dan in met %chmod%.</li>
  <li><strong>Als je toegang hebt via SSH</strong>, doe dan een <code>chmod %chmod% repertoire_courant</code>.</li>
  </ul>
  <p>Wanneer je dit gedaan hebt, zul je <strong><a href=\'%href%\'>deze bladzijde kunnen </a></strong> opladen om met de download en de installatie te beginnen.</p>
  <p>Als de fout voortduurt, zul je de klassieke installatieprocedure moeten gebruiken
  (download van alle bestanden met FTP).</p>',
	'titre' => 'Download van %paquet%',
	'titre_maj' => 'Aanpassing van %paquet%',
	'titre_version_courante' => 'Momenteel geïnstalleerde versie: ',
	'titre_version_future' => 'Installatie van versie: '
];
