<?php

return [
	'nom_langue' => 'òc niçard',

	// B
	'bouton_suivant' => 'Començar l’installacion >>',

	// C
	'ce_repertoire' => 'd’aqueu repertòri',

	// D
	'donnees_incorrectes' => '<h4>Marridas donadas. Vorgatz tornar provar, ò utilisar l’installacion manuala.</h4>
  <p>Error: %erreur%</p>',
	'du_repertoire' => 'dau repertòri',

	// E
	'echec_chargement' => '<h4>Lo cargament a capitat. Vorgatz tornar provar, ò utilisar l’installacion manuala.</h4>',

	// T
	'texte_intro' => '<p><strong>Benvenguda dins l’installacion automatica de %paquet%.</strong></p>
  <p>Lo systèma verifica en promier lu dreches d’accès au repertòri corrent.
  Es de lançar aüra lo telecargament dei donadas %paquet% dins aquest repertòri.</p>
  <p>Vorgatz clicar lo boton seguent per continuar.</p>', # MODIF
	'texte_preliminaire' => '<h2>Preliminari : <strong>Reglar lu dreches d’accès</strong></h2>
  <p><strong>Lo repertòri corrent es pas accessible en escritura.</strong></p>
  <p>Per l’arrengar, emplegatz lo voastre client FTP e cambiatz lu dreches d’accès
  d’aqueu repertòri (repertòri d’installacion de %paquet%).
  La guida d’installacion explica en detalhs lo biais de far. De chausir :</p>
<ul>
<li><strong>S’avètz un client FTP grafic</strong>, reglatz li proprietats dau repertòri corrent per que sigue accessible en escritura à toi.</li>
<li><strong>Se lo voastre client FTP es en mòde tèxto</strong>, cambiatz lo mòde dau repertòri à la valor %chmod%.</li>
<li><strong>S’avètz un accès SSH</strong>, faguètz un <code>chmod %chmod% repertori_corrent</code>.</li>
</ul>
<p>Un còup aquò fach podretz <strong><a href=\'%href%\'>tornar cargar aquesta pàgina</a></strong> per lançar lo telecargament pi l’installacion.</p>
<p>Se l’error demoara, vos caudrà emplegar lo biais d’installacion classic e (encargament de toi lu fichièrs per FTP).</p>',
	'titre' => 'Descargament de %paquet%'
];
