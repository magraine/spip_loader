<?php

return [
	'nom_langue' => 'brezhoneg',

	// B
	'bouton_suivant' => 'Kregiñ da staliañ >>',

	// C
	'ce_repertoire' => 'ar c’havlec’h-mañ',

	// D
	'donnees_incorrectes' => '<h4>Direizh eo ar roadennoù. Klaskit en-dro! pe grit gant ar staliadur dre-zorn.</h4>
  <p>Ar fazi a zo bet : %erreur%</p>',
	'du_repertoire' => 'ar c’havlec’h',

	// E
	'echec_chargement' => '<h4>C’hwitet eo ar c’hargañ. Klaskit en-dro, pe grit gant ar staliañ dre-zorn</h4>',

	// T
	'texte_intro' => '<p><strong>Degemer mat war benveg staliañ emgefreek %paquet%.</strong></p>
  <p>Gwiriet eo bet an aotreoù da dizhout ar c’havlec’h red,
  ha bremañ e vo pellgarget roadennoù %paquet% e %dest%.</p>
  <p>Klikit war an nozelenn da heul evit kenderc’hel ganti.</p>', # MODIF
	'texte_preliminaire' => '<h2>A-raok pep tra : 
<strong>Renkañ ar gwirioù dont tre</strong></h2>
<p><strong>N’eus ket tu skrivañ war ar c’havlec’h red.</strong></p>
<p>Implijit ho meziant FTP evit renkañ se dre gemmañ ar gwirioù dont tre
war ar c’havlec’h-mañ (an hini a staliit %paquet% warnañ).
Dre ar munud eo displeget penaos ober er sturlevr staliañ. Da zibab :</p>
<ul>
<li><strong>Ma rit gant ur meziant FTP grafek</strong>, kemmit perzhioù ar c’havlec’h red
a-benn ma c’hellfe forzh piv skrivañ warnañ.</li>
<li><strong>Ma rit gant ur meziant FTP e mod testenn</strong>, roit an talvoud %chmod% d’an teul.</li>
<li><strong>Ma rit gant TelNet</strong>, grit <code>chmod %chmod% repertoire_courant</code>.</li>
</ul>
<p>Pa vo bet graet an dra-se e vo tu deoc’h <strong><a href=\'%href%\'>adkargañ ar bajennad-mañ</a></strong>
 a-benn kregiñ gant ar pellgargañ, hag ar staliañ.</p>
<p>Ma chom ar fazi e vo dav deoc’h ober gant an doare klasel da staliañ (pellgargañ pep tra dre FTP).</p>',
	'titre' => 'Pellgargañ %paquet%'
];
