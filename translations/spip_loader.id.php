<?php

return [
	'nom_langue' => 'Indonesia',

	// B
	'bouton_suivant' => 'Instal >>',

	// C
	'ce_repertoire' => 'direktori ini',

	// D
	'donnees_incorrectes' => '<h4>Data tidak benar. Silakan coba
sekali lagi atau gunakan prosedur instalasi manual bila perlu.</h4>
  <p>Kesalahan : %erreur%</p>',
	'du_repertoire' => 'direktori',

	// E
	'echec_chargement' => '<h4>Proses unduh gagal. Silakan
mencoba lagi atau gunakan prosedur instalasi manual.</h4>',

	// T
	'texte_intro' => '<p><strong>Selamat datang di proses instalasi
otomatis %paquet%.</strong> <p>Pertama-tama, program akan memeriksa
hak akses pada direktori sekarang dan kemudian akan memulai proses unduh
berkas %paquet% ke %dest%. <p>Silakan klik tombol untuk melanjutkan proses.', # MODIF
	'texte_preliminaire' => '<h2>Langkah awal: <strong>Set hak akses</strong></h2>
<p><strong>Tidak dapat menulis di
direktori sekarang.</b</p>
<p>Untuk mengubah hak akses direktori di mana
anda akan menginstal %paquet% gunakan klien FTP anda.Prosedur dijelaskan secara rinci dalam manual instalasi. Pilih antara:</p>
<ul>
<li><strong>Jika anda memiliki sebiah klien FTP dengan tatap muka grafis</strong>, set hak akses
direktori untuk membuatnya terbuka bagi setiap orang untuk menulisnya.</li>
<li><strong>Jika anda memiliki sebuah klien FTP dengan tatap muka teks</strong>, ubah hak akses direktori ke nilai %chmod%.</li>
<li><strong>Jika anda menggunakan akses SSH</strong>,
jalankan perintah <code>chmod %chmod% direktori_sekarang</code>.</li>
</ul>
<p>Setelah ini dilakukan, silakan <strong><a href=\'%href%\'>perbaharui halaman ini</a></strong>
 untuk memulai proses unduh dan menginstal SPIP.</p>
<p>Jika anda menerima sebuah notifikasi kesalahan, anda harus melakukan proses instalasi secara manual (mengunduh berkas SPIP melalui FTP).</p>',
	'titre' => 'Unduh %paquet%'
];
