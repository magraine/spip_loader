<?php

return [
	'nom_langue' => 'Türkçe',

	// B
	'bouton_suivant' => 'Kuruluma başla >>',

	// C
	'ce_repertoire' => 'bu dizinin',

	// D
	'donnees_incorrectes' => '<h4>Veriler hatalı. Lütfen tekrar deneyiniz veya manüel olarak (el ile) kurunuz.</h4>
  <p>Oluşan hata: %erreur%</p>',
	'du_repertoire' => 'dizininin',

	// E
	'echec_chargement' => '<h4>Yüklemede başarısız. hata oluştu.  Le chargement a échoué. Lütfen tekrar deneyiniz veya manüel olarak (el ile) kurunuz.</h4>',

	// T
	'texte_intro' => '<p><strong>%paquet% paketinin otomatik kurulumuna hoşgeldiniz.</strong></p>
  <p>Sistem aktif dizine erişim haklarını kontrol etti.
  Şimdi %dest% dizinine %paquet% paketini indirme işlemini başlatacak.</p>
  <p>Lütfen devam etmek için "Sonraki" düğmesine basınız.</p>', # MODIF
	'texte_preliminaire' => '<h2>Ön bilgi : <strong>Erişim haklarını düzenleyiniz</strong></h2
<p><strong>Aktif dizine yazma izni yok.</strong></p>
<p>Bu sorunu çözmek için FTP programınızı kullanarak bu dizine erişim haklarını düzenleyiniz(%paquet% paketinin kurulum dizini).
Yordam kurulum kitapçığında detaylı olarak anlatılmıştır. Tercihinize göre :</p>
<ul>
<li><strong>eğer grafik tabanlı bir FTP programınız varsa</strong>, aktif dizinin niteliklerini herkese yazma hakkı verecek biçimde ayarlayınız.</li>
<li><strong>Eğer metin tabanlı bir FTP programınız varsa dizinin modunu %chmod% değerine getiriniz.</li>
<li><strong>Eğer SSH erişiminiz varsa</strong>, <code>chmod %chmod% aktif_dizin</code> komutunu çalıştırınız.</li>
</ul>
<p>Bu işlemi yaptıktan sonra <strong><a href=\'%href%\'>bu sayfayı yeniden yükle</a></strong>
 komutuyla indirmeyi başlatıp sonra kurulumu yapabilirsiniz.</p>
<p>Eğer hata tekrarlanırsa klasik kurulum yordamına geçmelisiniz (FTP ile tüm kurulum dosyalarının indirilmesi).</p>',
	'titre' => '%paquet% paketinin indirilmesi'
];
