<?php

return [
	'nom_langue' => 'òc lengadocian',

	// B
	'bouton_suivant' => 'Començar l’installacion >>',

	// C
	'ce_repertoire' => 'd’aquel repertòri',

	// D
	'donnees_incorrectes' => '<h4>Donadas incorrèctas. Volgatz tornar assajar, o emplegar l’installacion manuala.</h4>
  <p>Error: %erreur%</p>',
	'du_repertoire' => 'del repertòri',

	// E
	'echec_chargement' => '<h4>Lo cargament a abocat. Volgatz tornar assajar, o emplegar l’installacion manuala.</h4>',

	// T
	'texte_intro' => '<p><strong>Benvenguda dins l’installacion automatica de %paquet%.</strong></p>
  <p>D’en primièr, lo système a verificat los dreches d’accès al repertòri corrent,
  Ara va lançar lo telecargament de las donadas %paquet% dintre %dest%.</p>
  <p>Volgatz clicar lo boton seguent per continuar.</p>', # MODIF
	'texte_preliminaire' => '<h2>Preliminari : <strong>Reglar los dreches d’accès</strong></h2>
<p><strong>Lo repertòri corrent es pas accessible en escritura.</strong></p>
<p>Per o arrengar, emplegatz vòstre client FTP e cambiatz los dreches d’accès d’aquel repertòri (repertòri d’installacion de %paquet%).
La guida d’installacion explica en detalhs lo biais de far. De causir :</p>
<ul>
<li><strong>S’avètz un client FTP grafic</strong>, reglatz las proprietats del repertòri corrent per que siá accessible en escritura a totes.</li>
<li><strong>Se vòstre client FTP es en mòde tèxt</strong>, cambiatz lo mòde del repertòri a la valor %chmod%.</li>
<li><strong>S’avètz avez un accès SSH</strong>, fasètz un <code>chmod %chmod% repertori_corrent</code>.</li>
</ul>
<p>Un còp aquò fach pouiretz <strong><a href=\'%href%\'>tornar cargar aquesta pagina</a></strong> per lançar lo telecargament puèi l’installacion.</p>
<p>Se l’error demòra, vos caldrà emplegar lo biais d’installacion classic e
(encargament de totes los fichiers per FTP).</p>',
	'titre' => 'Telecargament de %paquet%'
];
