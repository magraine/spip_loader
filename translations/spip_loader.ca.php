<?php

return [
	'nom_langue' => 'català',

	// B
	'bouton_suivant' => 'Començar la instal·lació >>',

	// C
	'ce_repertoire' => 'd’aquest directori',

	// D
	'donnees_incorrectes' => '<h4>Dades incorrectes. Si us plau, intenteu-ho de nou o feu servir la instal·lació manual.</h4>',
	'du_repertoire' => 'del directori',

	// E
	'echec_chargement' => '<h4>La descàrrega ha fallat. Si us plau, intenteu-ho de nou o feu servir la instal·lació manual.</h4>',

	// T
	'texte_intro' => '<p><strong>Benvingut al procés d’instal·lació automàtic de %paquet%.</strong>
  <p>El sistema ha verificat els drets d’accés al directori actual,
  Ara començarà la baixada de dades %paquet% a l’interior %dest%.</p>
  <p>Premeu el següent botó per tal de continuar.</p>', # MODIF
	'texte_preliminaire' => '<h2>Preliminar: <strong>Ajustar els drets d’accés</strong></h2>
  <p><strong>El directori actual no té drets d’escriptura.</strong></p>
  <p>Per posar-hi remei, utilitzeu el vostre client FTP i ajusteu els drets d’accés
  a aquest directori (carpeta d’instal·lació de %paquet%).
  El procés està explicat detalladament a la guia d’instal·lació. A escollir:</p>
  <ul>
  <li><strong>Si teniu un client FTP gràfic</strong>, ajusteu les propietats del directori actual
  per tal que tots hi puguin escriure.</li>
  <li><strong>Si el vostre client FTP és en mode text</strong>, canvieu el mode del directori al valor %chmod%.</li>
  <li><strong>Si teniu un accés SSH</strong>, feu un <code>chmod %chmod% directori actual</code>.</li>
  </ul>
  <p>Un cop feta aquesta manipulació, podreu <strong><a href=\'%href%\'>recarregar aquesta pàgina</a></strong>
  per tal de començar la descàrrega i després la instal·lació.</p>
  <p>Si l’error persisteix, haureu de passar al procés d’instal·lació clàssic
  (descàrrega de tots els fitxers per FTP).</p>',
	'titre' => 'Descàrrega de %paquet%'
];
