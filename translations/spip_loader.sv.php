<?php

return [
	'nom_langue' => 'svenska',

	// B
	'bouton_suivant' => 'Installera >>',

	// C
	'ce_repertoire' => 'den här katalogen',

	// D
	'donnees_incorrectes' => '<h4>Felaktig uppgift. Var vänlig
försök igen eller använd den manuella installationen istället.</h4>
  <p>Fel : %erreur%</p>',
	'du_repertoire' => 'katalogen',

	// E
	'echec_chargement' => '<h4>Nedladdningen har misslyckats. Var vänlig
försök igen eller använd den manuella installationen istället.</h4>',

	// T
	'texte_intro' => '<p><strong>Välkommen till den automatiska
installationen av %paquet%.</strong> <p>Först kommer programmet att kontrollera
rättigheterna på den aktuella katalogen, sedan börjar den med nedladdningen av 
%paquet% filerna till %dest%. <p>Var vänlig, klicka på knappen för att fortsätta.', # MODIF
	'texte_preliminaire' => '<h2>Förberedande steg: <strong>Ställ in rättighetern för katalogen</strong></h2>
 <p><strong>Det är inte möjligt att skriva till den 
aktuella katalogen.</strong></p>
 <p>För att ändra rättigheterna för katalogen
du försöker installera %paquet% i, använd din FTP-klient. Hur du gör är beskrivet i detalj i installationshandledningen. Välj mellan:</p>
 <ul>
 <li><strong>Om du har en grafisk FTP-klient</strong>, sätt rättigheterna
för katalogen så att alla kan skriva till den.</li>
 <li><strong>Om du har en FTP-klient med textgränssnitt</strong>, ändra rättigheterna för katalogen till värdet %chmod%.</li>
 <li><strong>Om du använder en telnet- eller ssh-klient</strong>,
kör kommandot <code>chmod %chmod% aktuell_katalog</code>.</li>
 </ul>
<p>När det är gjort, var vänlig <strong><a href=\'%href%>uppdatera denna sida</a></strong>
 för att starta nedladdningen och installera SPIP.</p>
 <p>Om du fortfarande får det här flemeddelandet, måste du använda den manuella installationen
 (Ladda ned SPIP med FTP) istället.</p>',
	'titre' => 'Ladda ned %paquet%'
];
