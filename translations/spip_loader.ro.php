<?php

return [
	'nom_langue' => 'română',

	// B
	'bouton_suivant' => 'Începeţi instalarea >>',

	// C
	'ce_repertoire' => 'din acest director',

	// D
	'donnees_incorrectes' => '<h4>Date incorecte. Vă rugăm să încercaţi din nou sau să folosiţi instalarea manuală.</h4>
  <p>Eroare: %erreur%</p>',
	'du_repertoire' => 'din directorul',

	// E
	'echec_chargement' => '<h4>Încărcarea nu a fost reuşită. Vă rugăm să încercaţi din nou sau să folosiţi instalarea manuală.</h4>',

	// T
	'texte_intro' => '<p><strong>Bine aţi venit în procedura de instalare automatică a lui %paquet%.</strong></p>
  <p>Sistemul va verifica mai întâi drepturile de acces la directorul curent,
  şi după aceea va lansa descărcarea %paquet% în interiorul %dest%.</p>
  <p>Vă rugam să apăsaţi pe butonul următor pentru a continua.</p>', # MODIF
	'texte_preliminaire' => '<h2>Preliminar : <strong>Reglaţi drepturile de acces</strong></h2>
<p><strong>Directorul curent nu este accesibil pentru scriere.</strong></p>
<p>Pentru a corija aceasta, utilizaţi programul dumneavoastră preferat de FTP pentru a da drepturile de acces necesare în acest director (directorul de instalare %paquet%).
Procedura este explicata în delaiu în ghidul de instalare. Aveţi la alegere :
<ul>
<li><strong>Dacă aveţi un client FTP grafic</strong>, reglaţi proprietăţile directorului curent în aşa fel încât acesta să fie disponibil pentru scriere pentru toată lumea.</li>
<li><strong>Dacă clientul FTP este în mod text</strong>, schimbaţi modul de acces la directorul curent la valoarea %chmod%.</li>
<li><strong>Dacă aveţi un acces de tip shell</strong>, executaţi <code>chmod %chmod% director_curent</code>.</li>
</ul>
<p>O dată terminată această etapă, puteţi <strong><a href=\'%href%\'>reîncărca această pagină</a></strong> pentru a pèutea începe descărcarea şi după aceea instalarea.</p>
<p>Dacă erorile persistă, va trebuie să folosiţi procedura de instalarea clasică (descărcarea tuturor fişierelor prin FTP).</p>',
	'titre' => 'Descărcarea lui %paquet%'
];
