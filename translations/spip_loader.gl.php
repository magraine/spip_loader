<?php

return [
	'nom_langue' => 'galego',

	// B
	'bouton_suivant' => 'Comezar a instalación >>',

	// C
	'ce_repertoire' => 'deste cartafol',

	// D
	'donnees_incorrectes' => '<h4>Datos incorrectos. Faga un novo intento, ou utilice a instalación manual.</h4>
<p>Erro producido: %erreur%</p>',
	'du_repertoire' => 'do cartafol',

	// E
	'echec_chargement' => '<h4>A carga non vai. Inténteo de novo, ou utilice a instalación manual.</h4>  ',

	// T
	'texte_intro' => '<p><strong>Benvido/a ao proceso de instalación automática de %paquet%.</strong>
  <p>O sistema comprobou os permisos de acceso ao cartafol presente,
  agora, lanzará o proceso de carga dos datos de %paquet%  dentro deste cartafol.
  <p>Prema sobre o botón seguinte para continuar.', # MODIF
	'texte_preliminaire' => '<h2>Preliminar : 
  <strong>Regrar os permisos de acceso</strong></h2>
  <p><strong>O directorio actual non é accesíbel para escritura.</strong>
  <p>Para corrixilo, empregue o seu programa cliente de FTP co fin de establecer os permisos de acceso 
  para este cartafol (directorio de instalación de %paquet%).
  O procedemento está explicado detalladamente na guía de instalación. Escolla:
<ul>
  <li><strong>De ter un programa cliente de FTP gráfico</strong>, estableza as propiedades do directorio presente
  para que sexa accesíbel á escritura para todos.
  <p>
  <li><strong>De ter un cliente FTP en modo texto</strong>, troque o modo do cartafol e estableza o valor 777.
    <p>
  <li><strong>De ter un acceso SSH</strong>, faga un <code>chmod %chmod% directorio_presente</code>.<p>
  </ul>
  <p>Logo de realizar este axuste, poderá <strong><a href=\'%href%\'>refrescar esta páxina</A></strong>
  para comezar a carga e logo a instalación.</p>
  <p>Se o erro persiste, deberá pasar ao procedemento de instalación clásico
  (carga de todos os ficheiros por FTP).</p>',
	'titre' => 'Carga de %paquet%'
];
