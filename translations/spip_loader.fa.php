<?php

return [
	'nom_langue' => 'فارسى',

	// B
	'bouton_suivant' => 'نصب را شروع كنيد',

	// C
	'ce_repertoire' => 'از این رپرتوار',

	// D
	'donnees_incorrectes' => '<h4>داده های نادرست.خواهشمند است دوباره امتحان کنید و یا اینکه از نصب دستی استفاده کنید.</h4>
<p>اشتباه: %erreur%</p>',
	'du_repertoire' => 'از رپرتوار',

	// E
	'echec_chargement' => '<h4>بارگذاری انجام نشد. خواهشمند است دوباره امتحان کنید و یا اینکه از نصب دستی استفاده کنید. </h4> ',

	// T
	'texte_intro' => '<p><strong>به روند خودکار نصب %paquet% خوش آمدید.</strong></p>
 سیستم حقوق دسترسی به رپرتوار کنونی را وارسی کرد و بزودی داده های %paquet% را در درون %dest% بارگذاری میکند.<p> خواهشمند است دکمه ی زیر را برای ادامه ی روند فشار دهید.</p>', # MODIF
	'texte_preliminaire' => '<h2>گام نخست: <strong> مجوزهاي دسترسي را تنظيم كنيد</strong></h2>
<p><strong>نوشتن در ديركتوري فعلي ناممكن است.</strong></p>
<p> براي تغيير مجوز‌هاي ديركتوري‌اي كه مي‌خواهيد %paquet% را در آن نصب كنيد از اف.تي.پي خود استفاده كنيد.
روش كار با جزئيات در راهنماي تصوب گفته مي‌شود. انتخاب كنيد: </p>
<ul>
<li><strong>اگر اف.تي.پي با ظاهر گرافيكي داريد</strong>, مجوز‌هاي ديركتوري را طوري تنظيم كنيدكه براي نوشتن همه در آن باز باشد.</li>
<li><strong>اگر اف.تي.پي شما ظاهر متني دارد</strong>, مجوز‌هاي ديركتوري را به متغيير %chmod% تعيير دهيد.</li>
<li><strong> اگر يك ارتباط SSH داريد</strong>, اين فرمان را اجرا كنيد <code>chmod %chmod% repertoire_courant</code>.</li>
  </ul>
<p>هنگامي كه اجرا شد، لطفاً <strong><a href=\'%href%\'>اين صفحه را بازبارگذاي كنيد</a></strong>
براي انيكه بارگذاري و نصب را آغاز كنيد.</p>
<p>اگر اخطار خطا را دريافت كرديد، لازم است از روش نصب دستي استفاده كنيد
(بارگذاري تمام فايل‌هاي اسپپ با اف.تي.پي)..</p>',
	'titre' => 'بارگذاری %paquet%'
];
