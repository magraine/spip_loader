<?php

return [
	'nom_langue' => 'français copain',

	// B
	'bouton_suivant' => 'Commencer l’installation >>',

	// C
	'ce_repertoire' => 'de ce répertoire',

	// D
	'donnees_incorrectes' => '<h4>Données incorrectes. Essaye encore, ou passe à l’installation manuelle.</h4>
  <p>Erreur produite : %erreur%</p>',
	'du_repertoire' => 'du répertoire',

	// E
	'echec_chargement' => '<h4>Le chargement a échoué. Essaye encore, ou utilise l’installation manuelle.</h4>',

	// T
	'texte_intro' => '<p><strong>Bienvenue dans la procédure d’installation automatique de %paquet%.</strong></p>
  <p>Le système a vérifié les droits d’accès au répertoire courant.
  Il va lancer maintenant le téléchargement des données %paquet% à l’intérieur %dest%.</p>
  <p>Appuie sur le bouton suivant pour continuer.</p>', # MODIF
	'texte_preliminaire' => '<h2>Préliminaire : <strong>Régler les droits d’accès</strong></h2>
<p><strong>Le répertoire courant n’est pas accessible en écriture.</strong></p>
<p>Pour y remédier, utilise ton client FTP afin de régler les droits d’accès à ce répertoire (répertoire d’installation de %paquet%).
La procédure est expliquée en détail dans le guide d’installation. Au choix :</p>
<ul>
<li><strong>Si tu as un client FTP graphique</strong>, règle les propriétés du répertoire courant afin qu’il soit accessible en écriture pour tous.</li>
<li><strong>Si ton client FTP est en mode texte</strong>, change le mode du répertoire à la valeur %chmod%.</li>
<li><strong>Si tu as un accès SSH</strong>, fais un <code>chmod %chmod% repertoire_courant</code>.</li>
</ul>
<p>Une fois cette manipulation effectuée, tu pourras <strong><a href=\'%href%\'>recharger cette page</a></strong> afin de commencer le téléchargement puis l’installation.</p>
<p>Si l’erreur persiste, tu devras passer par la procédure d’installation classique (téléchargement de tous les fichiers par FTP).</p>',
	'titre' => 'Téléchargement de %paquet%'
];
