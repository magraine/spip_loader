<?php

return [
	'nom_langue' => 'slovenčina',

	// B
	'bouton_suivant' => 'Spustiť inštaláciu >>',
	'bouton_suivant_maj' => 'Spustiť aktualizáciu >>',

	// C
	'ce_repertoire' => 'z tohto úložiska',

	// D
	'donnees_incorrectes' => '<h4>Nesprávne údaje. Prosím,
skúste to znova alebo namiesto toho využite manuálnu inštaláciu.</h4>
  <p>Chyba: %erreur%</p>',
	'du_repertoire' => 'úložiska',

	// E
	'echec_chargement' => '<h4>Sťahovanie sa nepodarilo. Prosím,
skúste to znova alebo namiesto toho využite manuálnu inštaláciu.</h4>',
	'echec_php' => 'Verzia vášho PHP %php1% nie je kompatibilná s touto verziou SPIPu, ktorá si vyžaduje aspoň PHP %php2%.',

	// S
	'spip_loader_maj' => 'K dispozícii je spip_loader.php %version%.',

	// T
	'texte_intro' => '<strong>Program stiahne súbory zásuvného modulu %paquet% do %dest%.</p>',
	'texte_preliminaire' => '<h2>Predpríprava: <strong>Nastavenie prístupových povolení</strong></h2>
<p><strong>Do aktuálneho priečinka sa
nedá zapisovať.</strong></p
<p>Na zmenu povolení priečinka,
v ktorom inštalujete %paquet%, použite svojho FTP klienta. Postup je podrobne opísaný v inštalačnej príručke. Vyberte si:</p>
<ul>
<li><strong>Ak máte FTP klienta s grafickým rozhraním,</strong> nastavte povolenia
priečinka, aby sa otvoril pre každého, kto doň chce zapisovať.</li>
<li><strong>Ak máte FTP klienta s textovým rozhraním,</strong> zmeňte povolenia priečinka na hodnotu %chmod%.</li>
<li><strong>Ak využívate prístup cez SSH,</strong>
vykonajte príkaz <code>chmod %chmod% current_directory.</code></li>
</ul>
<p>Keď to urobíte, prosím <strong><a href=\'%href%\'>znova obnovte túto stránku,</a></strong>
 aby ste mohli začať so sťahovaním a inštaláciou SPIPu.</p>
<p>Ak sa vám stále zobrazuje toto hlásenie o chybe, inštaláciu budete musieť vykonať manuálne (stiahnuť súbory SPIPu cez FTP).</p>',
	'titre' => 'Stiahnuť %paquet%',
	'titre_maj' => '%paquet% bol aktualizovaný',
	'titre_version_courante' => 'Aktuálne nainštalovaná verzia: ',
	'titre_version_future' => 'Inštalácia verzie: '
];
