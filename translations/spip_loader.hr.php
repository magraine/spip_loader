<?php

return [
	'nom_langue' => 'hrvatski',

	// B
	'bouton_suivant' => 'Instalirati >>',

	// C
	'ce_repertoire' => 'ovaj direktorij',

	// D
	'donnees_incorrectes' => '<h4>Pogrešni podaci. Molim pokušajte
ponovo ili koristite ručnu proceduru instalacije.</h4>
  <p>Greška : %erreur%</p>',
	'du_repertoire' => 'direktorij',

	// E
	'echec_chargement' => '<h4>Preuzimanje nije uspjelo. Molim
Pokušajte ponovo ili koristite ručnu proceduru instalacije.</h4>',

	// T
	'texte_intro' => '<p><strong>Dobrodošli u automatski postupak
instalacije %paquet%.</strong> <p>Program će prvo provjeriti dozvole
aktuelnog direktorija i zatim početi preuzimanje 
%paquet% datoteka u %dest%. <p>Molim kliknite dugme za nastavak.', # MODIF
	'texte_preliminaire' => '<h2>Početni korak: <strong>Postavite dozvole pristupa</strong></h2>
<p><strong>Trenutno nije moguće pisati u aktuelni
direktorij.</strong></p>
<p>Za promijenu dozvole pristupa direktoriju u
koji instalirate %paquet% koristite Vaš FTP program.Taj postupak je detaljno objašnjen u uputi za instalaciju. Izaberite između:</p>
<ul>
<li><strong>Ako imate FTP program sa grafičkim sučeljem</strong>, promijenite dozvole pristupa tako 
da svako u njega može pisati.</li>
<li><strong>Ako imate FTP program sa tekstualnim sučeljem</strong>, promijenite dozvole za direktorij u vrijednost %chmod%.</li>
<li><strong>Ako koristite SSH pristup</strong>,
izvršite komandu <code>chmod %chmod% aktuelni_direktorij</code>.</li>
 </ul>
<p>Kada ste to učinili, <strong><a href=\'%href%\'>učitajte ovu stranicu ponovo</a></strong>
 da biste pokrenuli preuzimanje i instalirali %paquet%.</p>
 <p>Ako i pored toga budete dobijali ovu istu poruku, moraćete koristiti ručni metod instalacije
 (preuzimanje %paquet% datoteka pomoću FTP).</p>',
	'titre' => 'Preuzimanje %paquet%'
];
