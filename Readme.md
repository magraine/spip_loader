# SPIP Loader

Script permettant de télécharger ou mettre à jour SPIP via une interface web.

## Préambule

Si vous avez accès à un terminal, il est préférable d’installer SPIP avec Composer plutôt qu’avec cet outil.

## Usage

Copier le fichier `spip_loader.php` dans le répertoire de votre hébergement destiné à recevoir le site SPIP. Appeler le fichier depuis l’URL adaptée (ex: `https://domaine.tld/spip_loader.php`)

## Exigences

Ce Spip Loader nécessite :
- PHP 7.1 ou supérieur.
- le répertoire d’installation (souvent la racine du site) accessible en écriture.

Éventuellement, si différent, le répertoire contenant le spip_loader.php accessible en écriture également, pour permettre ses mises à jour automatiques.

## Configuration utilisateur

Il est possible de placer à côté du fichier `spip_loader.php` un fichier `spip_loader_config.php`,
retournant un tableau de configuration, sur le même modèle que le fichier `config/config.php` des sources du Spip loader. 

Notamment, cela permet d’autoriser des auteurs supplémentaires à l’utiliser

```php
<?php
return [
   'installation' => [
       'granted_authors' => [ 1, 5, 6 ],
   ],
];
```

Ou permet d’indiquer un répertoire d’installation spécifique
```php
<?php
return [
    'installation' => [
    	'granted_authors' => [ 1 ],
        'directory' => '/un/chemin/specifique',
    ],
];
```

# Développements

## Générer l’archive spip_loader.phar

Utiliser l’outil box.phar récupérable ici https://github.com/humbug/box
L’appeler depuis la racine de ces fichiers

- `php box.phar compile`

Archive sans suppression des commentaires ni altération des namespaces.

- `php box.phar compile --config=box.dev.json`

Cette action va générer l’archive `build/spip_loader.phar`

## Générer spip_loader.php, la signature et la version

- `php bin/post_box_build`

Cette action va générer les fichiers.

- `build/spip_loader.php`
- `build/spip_loader.sig`
- `build/spip_loader.version`

Le premier est une copie de l’archive .phar, avec l’extension .php,
Le second est la signature SHA384 du contenu de l’archive.

## Tester l’archive de dévoleppement en local

Créer ou modifier le fichier `spip_loader_config.php` pour indiquer des urls locales valides vers l’archive phar et la signature à utiliser :

```php
<?php
return [
	'debug' => true,
	'updates' => [
		'delay' => 5 * 60,
		'sources' => 'http://localhost/test/loader/build/',
	],
];
```

Où delay, est un délai en secondes entre 2 vérifications de mises à jour.