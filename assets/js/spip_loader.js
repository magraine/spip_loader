
class Ajax {
	static json(path) {
		return fetch(path, {
			method: 'GET',
			headers: {
				'X-Requested-With': 'XMLHttpRequest',
			}
		}).then(response => {
			return response.json();
		}).catch(e => {
			console.error(e);
		});
	}
	static stream(path, callable) {
		return fetch(path, {
			method: 'GET',
			headers: {
				'X-Requested-With': 'XMLHttpRequest',
			}
		}).then(response => {
			if (!response.ok) {
				throw Error(response.status+' '+response.statusText)
			}
			return new Response(
				new ReadableStream({
					async start(controller) {
						const reader = response.body.getReader();
						let string = "";
						let reg = /\<\!start\!\>(.*)\<\!end\!\>/m;
						while (true) {
							const {done, value} = await reader.read();
							string += new TextDecoder().decode(value).trim();
							console.log(string);
							if (string.length) {
								do {
									let s = string.match(reg);
									console.log(s);
									if (s) {
										string = string.replace(reg, '').trim();
										callable(done, s[1]);
										console.log(string);
									}
								} while (s);
							}

							if (done) {
								break;
							}
						}
						controller.close();
						reader.releaseLock();
					}
				})
			);
		}).then(response => {
			console.log(response);
			let text = response.text();
			console.log(text);
			return text;
		}).catch(e => {
			console.error(e);
			return {
				'success': false,
				'error': e,
			};
		});
	}
}

class Dom {
	static removeChilds(element) {
		while (element.firstChild) {
			element.removeChild(element.firstChild);
		}
	}
	static appendChilds(element, html) {
		element.insertAdjacentHTML('beforeend', html);
	}
	static addSpinner(element) {
		element.insertAdjacentHTML('beforeend', "<div class=\"spinner\"></div>");
	}
	static removeSpinner(element) {
		let spins = element.getElementsByClassName('spinner');
		while (spins[0]) {
			element.removeChild(spins[0]);
		}
	}
}

class Logger {
	constructor(debug = false) {
		this.debug = debug;
	}
	log(data) {
		if (this.debug) {
			console.log(data);
		}
	}
}


class SelfUpdater {
	constructor(options = {}) {
		this.logger = new Logger(options.debug || false);
		this.url_find = options.url_find;
		this.url_update = options.url_update;
		this.output = options.output;
		this.error = options.error;
	}

	findUpdate() {
		if (this.output) {
			Dom.addSpinner(this.output);
			Ajax.json(this.url_find).then(data => {
				this.logger.log(data);
				Dom.removeSpinner(this.output);
				if (data.success) {
					Dom.appendChilds(this.output, data.data.html);
					this.onClickUpdate();
				} else {
					console.error(data);
				}
			});
		}
	}
	onClickUpdate() {
		let updateLink = document.getElementById("do-selfupdate");
		if (updateLink) {
			updateLink.addEventListener('click', e => {
				e.preventDefault();
				this.doUpdate();
			});
		}
	}
	doUpdate() {
		if (this.output) {
			// spinner
			Dom.removeChilds(this.output);
			Dom.addSpinner(this.output);
			Ajax.json(this.url_update).then(data => {
				this.logger.log(data);
				Dom.removeSpinner(this.output);
				Dom.removeChilds(this.output);
				if (data.success) {
					Dom.appendChilds(this.output, data.data.html);
					document.location.reload(true);
				} else {
					Dom.appendChilds(this.output, this.error);
					console.error(data);
				}
			});
		}
	}
}

class Updater {
	constructor(options = {}) {
		this.logger = new Logger(options.debug || false);
		this.url_start = options.url_start;
		this.progress = options.progress;
		this.output = options.output;
		this.error = options.error;
	}
	start() {
		if (this.output) {
			Dom.addSpinner(this.output);
			Ajax.stream(this.url_start, function(done, value) {
				console.log("done", done);
				console.log("value", value, JSON.parse(value));
			}).then(data => {
				this.logger.log(data);
				Dom.removeSpinner(this.output);
				if (data.success) {
					Dom.appendChilds(this.output, data.data.html);
				} else {
					console.error(data);
				}
			});
		}
	}
}