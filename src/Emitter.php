<?php

namespace Spip\Loader;

use Psr\Http\Message\ResponseInterface;
use Spip\Loader\Message\IteratorStream;

class Emitter
{

    /**
     * Emits the given response
     *
     * @param ResponseInterface $response
     *
     * @return void
     */
    public function emit(ResponseInterface $response) : void
    {
        $headers = $response->getHeaders();

        foreach ($headers as $name => $values) {
            foreach ($values as $value) {
                \header(\sprintf(
                    '%s: %s',
                    $name,
                    $value
                ), false);
            }
        }

        \header(\sprintf(
            'HTTP/%s %d %s',
            $response->getProtocolVersion(),
            $response->getStatusCode(),
            $response->getReasonPhrase()
        ), true);

        $stream = $response->getBody();
        if ($stream instanceof IteratorStream) {
            foreach ($stream->getIterator() as $content) {
                echo $content;
                flush();
            }
        } else {
            echo $stream;
        }
    }
}
