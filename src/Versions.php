<?php

namespace Spip\Loader;

/**
 * Spip Versions and branch to update or install
 */
class Versions
{
    private $default_branch;
    private $asked_branch;
    private $package;
    private $branches;
    private $spip;
    private $url_archivelist = 'https://core.spip.net/projects/spip/repository/raw/archivelist.txt';

    public function __construct(Config $config, Spip $spip, $asked_branch = null)
    {
        $this->spip = $spip;
        $this->default_branch = $config->get('repository.default_branch');
        $this->asked_branch = $asked_branch;
        $this->package = $config->get('repository.package');
        $this->branches = $config->get('repository.branches');
    }

    /**
     * Current installed version
     */
    public function current() : string
    {
        $current = $this->spip->version();
        return $current ? 'SPIP ' . $current : "";
    }

    /**
     * Version that will be install
     */
    public function future() : string
    {
        $spip_versions = $this->get_spip_versions();
        $zip = $this->active_zip();
        $version_future = $spip_versions[$zip]['version'];
        if ($spip_versions[$zip]['etat'] == 'dev') {
            $version_future .= '-dev';
        }
        return 'SPIP ' . $version_future;
    }


    public function installable_branches() : array
    {
        $installed = $this->spip->branch();
        if (!$installed) {
            return $this->branches;
        }
        if ($installed === 'dev') {
            return array_intersect_key($this->branches, ['dev' => null]);
        }
        $branches = [];
        foreach ($this->branches as $branch => $desc) {
            if ($branch === 'dev' or version_compare($installed, $branch, "<=")) {
                $branches[$branch] = $desc;
            }
        }
        return $branches;
    }

    public function is_downloadable_branch($branch) : bool
    {
        return isset($this->branches[$branch]);
    }

    /**
     * Active branch, which will be download.
     * - The selected branch, needs to be downloadable
     * - Without selection : the current installed branch, if downloadable, unless default branch
     */
    public function active_branch() : string
    {
        $branch = $this->asked_branch;
        if (!$branch) {
            $branch = $this->spip->branch();
            if (!$branch or !$this->is_downloadable_branch($branch)) {
                $branch = $this->default_branch;
            }
        }
        if (!$this->is_downloadable_branch($branch)) {
            throw new \Exception(sprintf("Branch %s doesn’t exists.", $branch));
        }
        return $branch;
    }

    public function active_zip() : string
    {
        $branch = $this->active_branch();
        return $this->branches[$branch]['zip'];
    }

    /**
    * Renvoie un tableau des versions SPIP dont l'index correspond à au chemin du fichier zip tel
    * qu'utilisé par spip_loader
    */
    private function get_spip_versions()
    {
        static $versions = [];
        if (!empty($versions)) {
            return $versions;
        }

        // Récupération du fichier archivelist.txt du core
        $archivelist = file_get_contents($this->url_archivelist);
        $contenu = explode("\n", $archivelist);

        // on supprime les retours chariot
        $contenu = array_filter($contenu, 'trim');
        // on supprime les lignes vides
        $contenu = array_filter($contenu);

        if ($contenu) {
            // On lit le fichier ligne par ligne et on
            foreach ($contenu as $ligne) {
                if (substr($ligne, 0, 1) != '#') {
                    // C'est une ligne de definition d'un paquet :
                    $parametres = explode(';', $ligne);
                    // - et on extrait la version de spip du chemin svn
                    $arbo_svn = rtrim($parametres[0], '/');
                    $version = str_replace('spip-', '', basename($arbo_svn));
                    // - on separe calcul le nom complet du zip
                    $chemin = 'spip/' . $parametres[1] . '.zip';
                    // - on determine l'état de l'archive (stable, dev, archives)
                    $etat = substr($parametres[1], 0, strpos($parametres[1], '/'));
                    // Ajout au tableau des versions
                    $versions[$chemin] = [
                        'version' => $version,
                        'etat' => $etat];
                }
            }
        }

        return $versions;
    }
}
