<?php

namespace Spip\Loader\Translator;

/**
 * Translator
 *
 * Drastic cut of "symfony/translator" usage
 * to reduce size and files of .phar archive.
 * We use only php files and do not want plurals.
 */
class Translator
{
    private $fallback_locale = 'en';
    private $locale = 'en';
    private $default_domain = 'messages';

    private $resources = [];
    private $catalogue = [];

    public function __construct(string $locale = null, string $default_domain = null)
    {
        if ($locale) {
            $this->setFallbackLocale($locale);
            $this->setLocale($locale);
        }
        if ($default_domain) {
            $this->setDefaultDomain($default_domain);
        }
    }

    public function addResource($file, $locale, $domain = null)
    {
        if (!file_exists($file) or !is_readable($file)) {
            throw new \Exception("Translation file inexistant or unreadable");
        }
        if (null === $domain) {
            $domain = $this->getDefaultDomain();
        }
        $this->resources[$locale][$domain][] = $file;
    }

    public function trans($id, $parameters = [], $domain = null, $locale = null)
    {
        if (null === $domain) {
            $domain = $this->getDefaultDomain();
        }
        $id = (string) $id;
        $locale = $locale ?: $this->getLocale();
        $catalogue = $this->getCatalogue($locale, $domain);
        if (!$catalogue->offsetExists($id)) {
            if ($locale === $this->getFallbackLocale()) {
                return $id;
            }
            $catalogue = $this->getCatalogue($this->getFallbackLocale(), $domain);
            if (!$catalogue->offsetExists($id)) {
                return $id;
            }
        }

        $str = $catalogue->offsetGet($id);
        if ($parameters) {
            $str = str_replace(array_keys($parameters), $parameters, $str);
        }
        return $str;
    }

    private function getCatalogue(string $locale, string $domain) : \ArrayObject
    {
        if (!isset($this->catalogue[$locale][$domain])) {
            $messages = [];
            foreach ($this->resources[$locale][$domain] ?? [] as $file) {
                $messages = array_merge($messages, include $file);
            }
            $this->catalogue[$locale][$domain] = new \ArrayObject($messages);
        }
        return $this->catalogue[$locale][$domain];
    }

    public function setLocale(string $locale) : void
    {
        $this->locale = $locale;
    }

    public function getLocale() : string
    {
        return $this->locale;
    }

    public function getFallbackLocale() : string
    {
        return $this->fallback_locale;
    }

    public function setFallbackLocale(string $locale) : void
    {
        $this->fallback_locale = $locale;
    }

    public function getDefaultDomain(): string
    {
        return $this->default_domain;
    }

    public function setDefaultDomain(string $default_domain): void
    {
        $this->default_domain = $default_domain;
    }
}
