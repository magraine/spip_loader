<?php

namespace Spip\Loader\Helper;

class Files
{


    /**
     * Gestion des droits d'acces
     * @return [bool, int] : [writable, chmod]
     */
    public static function getChmodForWriteInDirectory($dir = null)
    {
        $self = $dir ?? basename($_SERVER['PHP_SELF']);
        $uid = @fileowner('.');
        $uid2 = @fileowner($self);
        $gid = @filegroup('.');
        $gid2 = @filegroup($self);
        $perms = @fileperms($self);

        // Comparer l'appartenance d'un fichier cree par PHP
        // avec celle du script et du repertoire courant
        $test = $self . '/test';
        @rmdir($test);
        @unlink($test); // effacer au cas ou
        @touch($test);
        if ($uid > 0 && $uid == $uid2 && @fileowner($test) == $uid) {
            $chmod = 0700;
        } else {
            if ($gid > 0 && $gid == $gid2 && @filegroup($test) == $gid) {
                $chmod = 0770;
            } else {
                $chmod = 0777;
            }
        }
        // Appliquer de plus les droits d'acces du script
        if ($perms > 0) {
            $perms = ($perms & 0777) | (($perms & 0444) >> 2);
            $chmod |= $perms;
        }
        @unlink($test);

        // Verifier que les valeurs sont correctes

        @mkdir($test, $chmod);
        @chmod($test, $chmod);
        $ok = (is_dir($test) && is_writable($test));
        @rmdir($test);

        return [$ok, $chmod];
    }
}
