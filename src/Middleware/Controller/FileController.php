<?php

namespace Spip\Loader\Middleware\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Spip\Loader\Middleware\BaseAbstractMiddleware;
use Sunrise\Http\Message\ResponseFactory;
use Sunrise\Stream\Stream;

class FileController extends BaseAbstractMiddleware
{

    /** Extensions / Mimes autorisés */
    private $mimes = [
        'css' => 'text/css',
        'js' => 'application/javascript',
        'png' => 'image/png',
        'jpg' => 'image/jpeg',
        'svg' => 'image/svg+xml',
    ];

    private $cache_duration = 24 * 60 * 60;

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $response = $handler->handle($request);

        $file = $request->getServerParams()['REQUEST_URI'];

        if (!$this->acceptFile($file)) {
            $response = (new ResponseFactory)->createResponse(404);
        } else {
            $path = $this->getPathFile($file);

            $response = $response
                ->withHeader('Content-Type', $this->getMimeType($path))
                ->withHeader('Content-Length', (string) filesize($path))
                ->withHeader('Expires', gmdate("D, d M Y H:i:s", time() + $this->cache_duration) . " GMT")
                ->withHeader('Pragma', 'cache')
                ->withHeader('Cache-Control', "max-age=$this->cache_duration")
                ->withBody(new Stream(fopen($path, 'r')));
        }

        return $response;
    }

    /** Accepter si pas .., extension tolérée et fichier présent */
    private function acceptFile($file) : bool
    {
        if (stripos($file, '..') !== false) {
            return false;
        }
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        if (!isset($this->mimes[$extension])) {
            return false;
        }
        return file_exists($this->getPathFile($file));
    }

    private function getPathFile($file)
    {
        return __DIR__ . '/../../..' . $file;
    }

    private function getMimeType($file) : string
    {
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        return $this->mimes[$extension] ?? '';
    }
}
