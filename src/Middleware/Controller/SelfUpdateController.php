<?php

namespace Spip\Loader\Middleware\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Spip\Loader\Exception\UpdaterException;
use Spip\Loader\Middleware\JsonableAbstractMiddleware;
use Spip\Loader\SelfUpdater;
use Sunrise\Http\Factory\ResponseFactory;

class SelfUpdateController extends JsonableAbstractMiddleware
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $response = $handler->handle($request);
        $query = $request->getAttribute('action');

        switch ($query) {
            case 'find':
                return $this->findUpdate($response);

            case 'update':
                return $this->selfUpdate($response);

            default:
                $response = (new ResponseFactory)->createResponse(400);
                return $this->htmlOrJsonResponse($response);
        }

        return $response;
    }

    private function findUpdate(ResponseInterface $response) : ResponseInterface
    {
        /** @var SelfUpdater $updater */
        $updater = $this->container->get('selfupdater');
        $html = "";
        if ($version = $updater->searchForUpdate()) {
            $html = $this->render('parts/selfupdate-link.html.twig', ['version' => $version]);
        }
        return $this->htmlOrJsonResponse($response, $html);
    }

    private function selfUpdate(ResponseInterface $response) : ResponseInterface
    {
        /** @var SelfUpdater $updater */
        $updater = $this->container->get('selfupdater');
        try {
            $updater->update();
        } catch (UpdaterException $e) {
            $response = (new ResponseFactory)->createResponse(400, $e->getMessage());
            return $this->htmlOrJsonResponse($response);
        } catch (\Exception $e) {
            if ($this->container->get('config')->get('debug')) {
                $response = (new ResponseFactory)->createResponse(400, $e->getMessage());
            } else {
                $response = (new ResponseFactory)->createResponse(400, "An error occured");
            }
            return $this->htmlOrJsonResponse($response);
        }
        return $this->htmlOrJsonResponse($response, '<span class="text-success">Mise à jour effectuée.</span>');
    }
}
