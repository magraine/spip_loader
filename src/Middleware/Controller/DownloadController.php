<?php

namespace Spip\Loader\Middleware\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Spip\Loader\Helper\Files;
use Spip\Loader\Message\IteratorStream;
use Spip\Loader\Middleware\JsonableAbstractMiddleware;
use Sunrise\Http\Factory\ResponseFactory;

class DownloadController extends JsonableAbstractMiddleware
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $response = $handler->handle($request);

        if ($this->isJsonResponse()) {
            $query = $request->getAttribute('action');
            switch ($query) {
                case 'start':
                    return $this->start($request, $response);
                default:
                    $response = (new ResponseFactory)->createResponse(400);
                    return $this->htmlOrJsonResponse($response);
            }
        }

        return $this->index($request, $response);
    }

    /**
     * Retourne l’index de la page de download, qui chargera les requêtes ajax nécessaires.
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    private function index(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface
    {
        $dir = $this->container->get('config')->get('installation.directory');
        list($writable, $chmod) = Files::getChmodForWriteInDirectory($dir);
        if (!$writable) {
            $html = $this->render('access.html.twig', ['chmod' => sprintf('%04o', $chmod)]);
        } else {
            $html = $this->render('download.html.twig', [
                'versions' => $this->getVersions($request)
            ]);
        }

        $response->getBody()->write($html);
        return $response;
    }


    private function start(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface
    {
        $versions = $this->getVersions($request);

        $response = $this->prepareStreaming($response);
        $stream = new IteratorStream($this->startDownloadGenerator());
        $response = $response->withBody($stream);
        return $response;
    }

    private function startDownloadGenerator()
    {
        $buffer_size = 8192;
        $start = "<!start!>";
        $end = "<!end!>";
        $iter = 5;

        for ($i = 0; $i < $iter; $i++) {
            \sleep(1);
            $message = \json_encode([
                'progress' => $i,
                'log' => "On arrive à $i",
            ]);
            $message = \str_pad($start . $message . $end . PHP_EOL, $buffer_size);

            yield $message;
            flush();
        }
    }

    /**
     * Prépare le serveur pour envoyer des informations en streaming
     *
     * Tente de squizer les buffers, compressions, à différents endroits (php, apache, nginx…)
     *
     * @link http://stackoverflow.com/questions/7740646/jquery-ajax-read-the-stream-incrementally
     * @link http://www.jeffgeerling.com/blog/2016/streaming-php-disabling-output-buffering-php-apache-nginx-and-varnish
     * @link https://phpfashion.com/everything-about-output-buffering-in-php
     **/
    private function prepareStreaming(ResponseInterface $response) : ResponseInterface
    {
        $response = $response
            ->withHeader('Content-type', 'text/html; charset=utf-8')
            // Explicitly disable caching so Varnish and other upstreams won't cache.
            ->withHeader('Cache-Control', 'no-cache, must-revalidate')
            // Setting this header instructs Nginx to disable fastcgi_buffering and disable gzip for this request.
            ->withHeader('X-Accel-Buffering', 'no');

        // Turn off output buffering
        ini_set('output_buffering', 'Off');
        // Turn off PHP output compression
        ini_set('zlib.output_compression', false);
        // Implicitly flush the buffer(s)
        ini_set('implicit_flush', true);
        ob_implicit_flush(true);

        // Clear, and turn off output buffering
        while (ob_get_level() > 0) {
            // Get the curent level
            $level = ob_get_level();
            // End the buffering
            ob_end_clean();
            // If the current level has not changed, abort
            if (ob_get_level() == $level) {
                break;
            }
        }

        // Disable apache output buffering/compression
        if (function_exists('apache_setenv')) {
            apache_setenv('no-gzip', '1');
            apache_setenv('dont-vary', '1');
        }

        return $response;
    }
}
