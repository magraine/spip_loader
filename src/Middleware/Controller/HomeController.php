<?php

namespace Spip\Loader\Middleware\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Spip\Loader\Helper\Files;
use Spip\Loader\Middleware\BaseAbstractMiddleware;

class HomeController extends BaseAbstractMiddleware
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        $response = $handler->handle($request);

        $dir = $this->container->get('config')->get('installation.directory');
        list($writable, $chmod) = Files::getChmodForWriteInDirectory($dir);
        if (!$writable) {
            $html = $this->render('access.html.twig', ['chmod' => sprintf('%04o', $chmod)]);
        } else {
            $html = $this->render('index.html.twig', [
                'versions' => $this->getVersions($request)
            ]);
        }

        $response->getBody()->write($html);

        return $response;
    }
}
