<?php

namespace Spip\Loader\Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Spip\Loader\Versions;

abstract class BaseAbstractMiddleware implements MiddlewareInterface
{
    /** @var ContainerInterface */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    abstract public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface;


    protected function render($file, $data = [])
    {
        if (!$this->container->has('twig')) {
            throw new \LogicException("Twig is needed to render a template.");
        }
        $twig = $this->container->get('twig');
        return $twig->render($file, $data);
    }

    protected function getVersions(ServerRequestInterface $request)
    {
        return new Versions(
            $this->container->get('config'),
            $this->container->get('spip'),
            $request->getQueryParams()['branch'] ?? null
        );
    }
}
