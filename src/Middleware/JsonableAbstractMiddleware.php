<?php

namespace Spip\Loader\Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

abstract class JsonableAbstractMiddleware extends BaseAbstractMiddleware
{
    protected $jsonResponse = false;

    public function __construct(ContainerInterface $container, bool $jsonResponse = false)
    {
        parent::__construct($container);
        $this->jsonResponse = $jsonResponse;
    }

    protected function isJsonResponse() : bool
    {
        return $this->jsonResponse;
    }

    protected function htmlOrJsonResponse(ResponseInterface $response, $html = null, $json = null) : ResponseInterface
    {
        if ($this->isJsonResponse()) {
            if ($json === null && $html !== null) {
                $json = ['html' => $html];
            }
            return $this->jsonResponse($response, $json);
        }
        if ($html !== null) {
            $response->getBody()->write($html);
        }
        return $response;
    }

    protected function jsonResponse(ResponseInterface $response, $data = null) : ResponseInterface
    {
        $response->withHeader('Content-Type', 'application/json');
        if ($response->getStatusCode() === 200) {
            return $this->success($response, $data);
        } else {
            return $this->error($response, $data);
        }
    }

    private function success(ResponseInterface $response, $data = null) : ResponseInterface
    {
        $response->getBody()->write(\json_encode([
            'success' => true,
            'status' => 200,
            'error' => '',
            'data' => $data,
        ]));
        return $response;
    }

    private function error(ResponseInterface $response, $data = null) : ResponseInterface
    {
        $response->getBody()->write(\json_encode([
            'success' => false,
            'status' => $response->getStatusCode(),
            'error' => $response->getReasonPhrase(),
            'data' => $data,
        ]));
        return $response;
    }
}
