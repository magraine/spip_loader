<?php

namespace Spip\Loader\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Spip\Loader\Spip;
use Sunrise\Http\Factory\ResponseFactory;

class AccesMiddleware extends BaseAbstractMiddleware
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler) : ResponseInterface
    {
        /** @var Spip $spip */
        $spip = $this->container->get('spip');
        if (!$spip->isGranted()) {
            $response = (new ResponseFactory())->createResponse(403, 'Access Denied');
            $response->getBody()->write($this->render('403.html.twig'));
            return $response;
        }

        return $handler->handle($request);
    }
}
