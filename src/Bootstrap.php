<?php

namespace Spip\Loader;

if (\PHP_SAPI === 'cli') {
    throw new \Exception("This is only a web application. Please use Composer to install SPIP using a console.");
}

if (file_exists(__DIR__ . '/../vendor/autoload.php')) {
    require_once __DIR__ . '/../vendor/autoload.php';
} elseif (file_exists(__DIR__ . '/../../../autoload.php')) {
    require_once __DIR__ . '/../../../autoload.php';
} else {
    throw new \Exception("Can't find autoloader. Need Composer install ?");
}

$app = new Application();
$app->run();
