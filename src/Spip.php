<?php

namespace Spip\Loader;

/** Current SPIP */
class Spip
{
    private $racine;
    private $version_file = '/ecrire/inc_version.php';
    private $bootstrap_file = '/ecrire/inc_version.php';
    private $granted_authors;

    public function __construct(Config $config)
    {
        $this->racine = $config->get('installation.directory');
        $this->granted_authors = (array)$config->get('installation.granted_authors');
    }

    public function exists() : bool
    {
        return @file_exists($this->racine . $this->version_file);
    }

    public function url_redirect() : string
    {
        if ($this->exists()) {
            return "ecrire/?exec=accueil";
        } else {
            return "ecrire/?exec=install";
        }
    }

    public function version() : string
    {
        if ($this->exists()) {
            $content = file_get_contents($this->racine . $this->version_file);
            // $spip_version_branche = "3.2.1";
            if (preg_match('#[$]spip_version_branche = "(.+)";#', $content, $m)) {
                return $m[1];
            }
        }
        return "";
    }

    public function branch() : string
    {
        if ($version = $this->version()) {
            if (stripos($version, '-dev')) {
                return 'dev';
            }
            $v = explode('.', $version);
            return $v[0] . '.' . ($v[1] ?? '0');
        }
        return "";
    }

    /**
     * If SPIP is already installed, you need to be a registered webmaster
     * to use this tool.
     */
    public function isGranted()
    {
        if (!$this->exists()) {
            return true;
        }

        # Start SPIP :/
        include_once $this->racine . $this->bootstrap_file;

        if (!isset($GLOBALS['auteur_session']['statut']) or
            $GLOBALS['auteur_session']['statut'] != '0minirezo' or
            !in_array($GLOBALS['auteur_session']['id_auteur'], $this->granted_authors)
        ) {
            return false;
        }
        return true;
    }
}
