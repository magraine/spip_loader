<?php

namespace Spip\Loader;

use Spip\Loader\Exception\UpdaterException;

class SelfUpdater
{
    private $url;
    private $phar_file = 'spip_loader.phar';
    private $phar_sig = 'spip_loader.sig';
    private $phar_version = 'spip_loader.version';

    private $local_version;
    private $local_directory;
    private $local_phar;
    private $local_phar_tmp;
    private $delay_update;

    public function __construct(Config $config)
    {
        $this->url = rtrim($config->get('updates.sources'), '/') . '/';
        $this->delay_update = $config->get('updates.delay');
        $this->local_version = $config->get('application.version');
        $this->local_directory = $config->get('application.directory');
        $this->local_phar = $this->local_directory . '/' . $config->get('application.filename');
        $this->local_phar_tmp = $this->local_phar . '.tmp';
    }

    /**
     * @return string|false : the new version name, or false.
     */
    public function searchForUpdate()
    {
        if ($this->amIOld()) {
            $version = $this->getVersionIfNewer();
            if ($version) {
                return $version;
            }
            $this->touchMe();
        }
        return false;
    }

    public function update() : bool
    {
        if (!$this->writable()) {
            throw new UpdaterException("SPIP Loader can’t be rewriten.");
        }
        $signature = $this->getExpectedSignature();
        if (!$signature) {
            return true;
        }
        if ($signature === $this->getSignature($this->local_phar)) {
            return true;
        }
        return $this->download();
    }

    /** Download phar in temporary place */
    private function download() : bool
    {
        if (file_exists($this->local_phar_tmp)) {
            \unlink($this->local_phar_tmp);
        }
        $expected_signature = $this->getExpectedSignature();
        if (!$expected_signature) {
            throw new UpdaterException("SPIP Loader signature download fails. Try later.");
        }
        copy($this->url . $this->phar_file, $this->local_phar_tmp);
        if (!file_exists($this->local_phar_tmp)) {
            throw new UpdaterException("SPIP Loader Phar download fails. Try later.");
        }
        if ($expected_signature !== $this->getSignature($this->local_phar_tmp)) {
            \unlink($this->local_phar_tmp);
            throw new UpdaterException("SPIP Loader Phar has wrong signature. Try later.");
        }
        return $this->replace($expected_signature);
    }

    /** Replace local phar with the tmp phar just downloaded */
    private function replace(string $expected_signature)
    {
        $old_signature = $this->getSignature($this->local_phar);
        copy($this->local_phar_tmp, $this->local_phar);
        $new_signature = $this->getSignature($this->local_phar);
        if ($expected_signature !== $new_signature) {
            \unlink($this->local_phar_tmp);
            // What happens ?
            if ($old_signature !== $new_signature) {
                \unlink($this->local_phar);
            }
            throw new UpdaterException("SPIP Loader Phar has wrong signature. Try later.");
        }
        return true;
    }

    public function writable()
    {
        return is_writable($this->local_directory) && is_writable($this->local_phar);
    }


    private function amIOld() : bool
    {
        if (\time() - \filemtime($this->local_phar) > $this->delay_update) {
            return true;
        }
        return false;
    }

    private function getVersionIfNewer()
    {
        $version = $this->getDistantVersion();
        if ($version and $version !== $this->local_version) {
            return $version;
        }
        return false;
    }

    private function getDistantVersion()
    {
        return \file_get_contents($this->url . $this->phar_version);
    }

    private function touchMe()
    {
        if (\is_writeable($this->local_phar)) {
            \touch($this->local_phar);
        }
    }

    private function getExpectedSignature()
    {
        static $signature = null;
        if (null === $signature) {
            $signature = \file_get_contents($this->url . $this->phar_sig);
        }
        return $signature;
    }

    private function getSignature(string $file) : string
    {
        return \hash_file('SHA384', $file);
    }
}
