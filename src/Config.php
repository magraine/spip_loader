<?php

namespace Spip\Loader;

use xy2z\LiteConfig\LiteConfig;

class Config
{
    public function __construct(string $file)
    {
        $this->loadFile($file);
    }

    public function loadFile(string $file)
    {
        LiteConfig::loadFile($file);
    }

    public function has(string $key)
    {
        return LiteConfig::exists($key);
    }

    public function get(string $key)
    {
        return LiteConfig::get($key);
    }

    /* help Twig */
    public function __get($key)
    {
        return $this->get($key);
    }

    /* help Twig */
    public function __isset($key)
    {
        return $this->has($key);
    }
}
