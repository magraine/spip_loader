<?php

namespace Spip\Loader;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Simplex\Container;
use Spip\Loader\Middleware\AccesMiddleware;
use Spip\Loader\Middleware\Controller\DownloadController;
use Spip\Loader\Middleware\Controller\FileController;
use Spip\Loader\Middleware\Controller\HomeController;
use Spip\Loader\Middleware\Controller\SelfUpdateController;
use Spip\Loader\Translator\Translator;
use Sunrise\Http\Factory\ResponseFactory;
use Sunrise\Http\Router\Exception\MethodNotAllowedException;
use Sunrise\Http\Router\Exception\RouteNotFoundException;
use Sunrise\Http\Router\RouteCollection;
use Sunrise\Http\Router\RouteCollectionInterface;
use Sunrise\Http\Router\Router;
use Sunrise\Http\Router\RouterInterface;
use Sunrise\Http\ServerRequest\ServerRequestFactory;
use Twig_Environment;
use Twig_Extension_Debug;
use Twig_Filter;
use Twig_Loader_Filesystem;

class Application
{
    /** @var Container */
    protected $container;

    public function __construct()
    {
        if (false === ini_get('date.timezone')) {
            date_default_timezone_set('UTC');
        }
        $this->container = new Container();
        $this->configureConfig($this->container);
        $this->configureServices($this->container);
        $this->configureRoutes($this->container->get('router'));
    }

    /**
     * Runs the application.
     */
    public function run()
    {
        $this->manageUri();
        $request = ServerRequestFactory::fromGlobals();
        $request = $this->init($request);
        $response = $this->handle($request);
        $emitter = new Emitter();
        $emitter->emit($response);
    }

    /**
     * Get config, from Application and User overwrites
     */
    private function configureConfig(ContainerInterface $container)
    {
        $config = new Config(__DIR__ . '/../config/config.php');
        $user_config_file = $config->get('application.directory') . DIRECTORY_SEPARATOR . $config->get('application.user_config_filename');
        if (file_exists($user_config_file)) {
            $config->loadFile($user_config_file);
        }
        $container->set('config', $config);
    }

    /**
     * Declare services (translator, twig, router...)
     */
    private function configureServices(ContainerInterface $container)
    {
        $container->set('router', new Router());
        $container->set('spip', function ($container) {
            return new Spip($container->get('config'));
        });
        $container->set('selfupdater', function ($container) {
            return new SelfUpdater($container->get('config'));
        });
        $this->configureTranslations($container);
        $this->configureTwig($container);
    }

    private function configureTranslations(ContainerInterface $container)
    {
        // Traductions
        $translator = new Translator(
            $container->get('config')->get('application.translations.default_locale'),
            $container->get('config')->get('application.translations.default_domain')
        );
        // Ne pas utiliser Glob ou GlobIterator ; ne fonctionne pas avec Phar.
        $locales = [];
        foreach ((new \FilesystemIterator(__DIR__ . '/../translations', \FilesystemIterator::SKIP_DOTS)) as $file) {
            if ($file->getExtension() !== 'php') {
                continue;
            }
            list($domain, $locale) = \explode('.', $file->getBasename('.php'));
            $translator->addResource($file, $locale, $domain);
            $locales[] = $locale;
        }
        sort($locales);
        $container->set('translator', $translator);
        $container->set('locales', \array_unique($locales));
    }

    private function configureTwig(ContainerInterface $container)
    {
        // Templates Twig
        $config = $container->get('config');
        $translator = $container->get('translator');
        $twig = new Twig_Environment(
            new Twig_Loader_Filesystem(__DIR__ . '/../templates')
        );
        if ($config->get('debug')) {
            $twig->enableDebug();
            $twig->addExtension(new Twig_Extension_Debug());
        }
        $twig->addGlobal('config', $config);
        $twig->addGlobal('locales', $this->container->get('locales'));
        $twig->addFilter(new Twig_Filter('trans', function ($message, array $arguments = [], $domain = null, $locale = null, $count = null) use ($translator) {
            if (null !== $count) {
                $arguments['%count%'] = $count;
            }
            return $translator->trans($message, $arguments, $domain, $locale);
        }));
        $twig->addFunction(new \Twig_Function('asset', function ($file) {
            return "?asset=$file";
        }));
        $twig->addFunction(new \Twig_Function('ajax', function ($context, $page) {
            return "?ajax=$page&branch=" . $context['versions']->active_branch();
        }, ['needs_context' => true]));
        $twig->addFunction(new \Twig_Function('page', function ($context, $page) {
            return "?page=$page&branch=" . $context['versions']->active_branch();
        }, ['needs_context' => true]));
        $container->set('twig', $twig);
    }

    /**
     * Declare routes
     * Routes are tested throught 'REQUEST_URI' which is altered for .phar usage
     * by manageUri().
     */
    private function configureRoutes(RouterInterface $router)
    {
        $routes = new RouteCollection();
        $access = new AccesMiddleware($this->container);

        $routes
            ->get('home', '/')
            ->addMiddleware($access)
            ->addMiddleware(new HomeController($this->container));

        $routes->group('/ajax', function (RouteCollectionInterface $routes) use ($access) {
            $routes->get('self.update', '/self-update/{action}')
                ->addPattern('action', '\w+')
                ->addMiddleware($access)
                ->addMiddleware(new SelfUpdateController($this->container, true));

            $routes->get('download', '/download/{action}')
                ->addPattern('action', '\w+')
                ->addMiddleware($access)
                ->addMiddleware(new DownloadController($this->container, true));
        });

        $routes
            ->get('assets', '/assets/{file}')
            ->addPattern('file', '.*')
            ->addMiddleware(new FileController($this->container));

        $routes
            ->get('download', '/download')
            ->addMiddleware($access)
            ->addMiddleware(new DownloadController($this->container));

        $router->addRoutes($routes);
    }


    /**
     * Route query string to REQUEST_URI for .phar to work with the router.
     * - ?asset=img/logo.svg => /assets/img/logo.svg
     * - ?page=demo => /demo
     */
    private function manageUri()
    {
        if (isset($_GET['asset'])) {
            $_SERVER['REQUEST_URI'] = '/assets/' . $_GET['asset'];
        } elseif (isset($_GET['ajax'])) {
            $_SERVER['REQUEST_URI'] = '/ajax/' . $_GET['ajax'];
        } else {
            $_SERVER['REQUEST_URI'] = '/' . ($_GET['page'] ?? '');
        }
    }

    /**
     * Init infos from request
     */
    private function init(ServerRequestInterface $request) : ServerRequestInterface
    {
        $locale = $request->getCookieParams()['locale'] ?? $this->container->get('config')->get('application.translations.default_locale');
        /** @var Translator $translator */
        $translator = $this->container->get('translator');
        $translator->setLocale($locale);
        /** @var Twig_Environment $twig */
        $twig = $this->container->get('twig');
        $twig->addGlobal('lang', [
            'locale' => $locale,
            'dir' => in_array($locale, ['ar', 'he', 'fa']) ? 'rtl' : 'ltr',
        ]);
        return $request;
    }

    /**
     * Get response from request
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    private function handle(ServerRequestInterface $request) : ResponseInterface
    {
        try {
            $response = $this->container->get('router')->handle($request);
        } catch (RouteNotFoundException $e) {
            $response = (new ResponseFactory)->createResponse(404);
            $response->getBody()->write($response->getReasonPhrase());
        } catch (MethodNotAllowedException $e) {
            $response = (new ResponseFactory)->createResponse(405)
                ->withHeader('allow', implode(', ', $e->getAllowedMethods()));
            $response->getBody()->write($response->getReasonPhrase());
        }

        return $response;
    }
}
