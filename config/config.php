<?php

return [
    'installation' => [
        'granted_authors' => [ 1 ],
        'directory' => dirname($_SERVER['SCRIPT_FILENAME']),
    ],
    'debug' => false,
    'application' => [
        'filename' => basename($_SERVER['SCRIPT_FILENAME']),
        'directory' => dirname($_SERVER['SCRIPT_FILENAME']),
        'user_config_filename' => 'spip_loader_config.php',
        'version' => file_get_contents(__DIR__ . '/../VERSION'),
        'translations' => [
            'default_locale' => 'fr',
            'default_domain' => 'spip_loader',
        ],
    ],
    'updates' => [
        'delay' => 5 * 60,
        'sources' => 'https://spip.net/INSTALL/',
    ],
    'repository' => [
        'package' => 'SPIP',
        'default_branch' => '3.2',
        'branches' => [
            'dev' => [
                'zip' => 'spip/dev/SPIP-svn.zip',
                'php' => '5.4.0',
            ],
            '3.2' => [
                'zip' => 'spip/stable/spip-3.2.zip',
                'php' => '5.4.0',
            ],
            '3.1' => [
                'zip' => 'spip/stable/spip-3.1.zip',
                'php' => '5.1.0',
            ],
            '3.0' => [
                'zip' => 'spip/stable/spip-3.0.zip',
                'php' => '5.1.0',
            ],
        ],
        'zip_remove_path' => 'spip',
    ],
];
